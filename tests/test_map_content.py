import unittest
from src.dofus_tools.Maps.Model.Content.Tile import Tile
from src.dofus_tools.Maps.Model.Content.Tile import Movement
from src.dofus_tools.Maps.Model.Content import Content, CoordinatesSystem
from src.dofus_tools.Tools import ascii2b64e


class TestCoordinatesSystem(unittest.TestCase):
    def test_dimensions_size(self):
        self.assertEqual(479, len(CoordinatesSystem((15, 17))))

    def test_index_to_coordinates(self):
        cs = CoordinatesSystem((15, 17))
        self.assertEqual((14, -12), cs(42))
        self.assertEqual((19, 6), cs(369))

    def test_coordinates_to_index(self):
        cs = CoordinatesSystem((15, 17))
        self.assertEqual(42, cs((14, -12)))
        self.assertEqual(369, cs((19, 6)))

    def test_neighbors_coordinates(self):
        expected_neighbors = {(0, 1), (1, 0)}
        neighbors = set(CoordinatesSystem((15, 17)).neighbors((0, 0)))

        self.assertEqual(expected_neighbors, neighbors)

    def test_neighbors_indices(self):
        expected_neighbors = {14, 15}
        neighbors = set(CoordinatesSystem((15, 17)).neighbors(0))

        self.assertEqual(expected_neighbors, neighbors)


class TestCell(unittest.TestCase):
    def test_decode_encode(self):
        # 2073_0706141524X.swf (Goball dungeon, room 1)
        map_data = "bhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaGhaaeaaatSHhaaeaaatPHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaetPHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes8aaaHhaaeaaaaaHhaaeaaaaaHhaaes8aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaaHhaaeaaaaaGhaaeaaatVHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhN2eaaaaaHhaaeaaaaaHhaaeaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhN2eaaaaaHhN_eaaaaaHhb6es-aaaHhN2eaaaqKHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhN2eaaaaaHhN_eaaaaaH3N9eaaaaaH3N7eaaaaaHhN2eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaGhaaeaaatSHhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHxN_eaaaaaHhN_eaaaaaHhH6eaNWaaHNN9eaaaaaHNN9eaaaaaHhN2eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhHfeaaaqvHNN9eaaaaaHhH6eaJWaaHhH6eaNaaaHhHfeaaaaaHxN_eaaaaaHhN2eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhHfeaaaaaHhH6eaNWaaHhH6eaJWaaHhH6eaJqaaHhGNeaaaaaHhHfem0aaaHxN_em0aaaH3N7eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhH6eaJaaaHhH6eaJWaaHhH6em0aaaHhH6eaJqaaHhHfeaaaaaHhHfeaaaaaHhH2eaNWaaHNN9eaaaaaHxN_eaaaaaHhN_etFaaaHhaaes6aaaHhaaeaaaaabhGaeaaaaaHhaaes6iaaHhH6etHiaaHhH6em1aaaHhH6eaaaaaHhH6eaaaaaHhHfeaaaaaHhH6eaNWaaHhH6eaJWaaHhH2eaJaaaHhH6eaNWaaHNN9eaaaaaHhGNetHaaaHhaaes6aaaHhaaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaJWaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaNWaaHhHfeaaaaaHhaaes6aaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6em0aaaHhH6eaaaaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaJWaaHhaaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaJqaaHhH6eaJGaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhaaetbaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhGNeaaaaaHhH6eaJGaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaNaaaHhHfeaaaaaHhH6eaaaaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhHfeaaaaaHhH6eaJWaaHhH6eaaaaaHhH6eaaaaaHhH6eaJqaaHhH6eaNqaaHhH6eaJGaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaJWaaHhH6eaaaaaHhH6eaaaaaHhH6eaJqaaHhGNeaaaaaHhHfeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6em1aaaHhH6eaaaaaHhH2eaaaaaHhHfeaaaaaHhHfeaaaaaHhH6eaNGaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaNaaaHhGNem0aaaHhHfeaJaqKHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhHfeaaaaaHhHfeaaaaaHhH6eaNGaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaGhb6eaaetLHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaNWaaHhH6eaNWaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaGhb6eaaetLHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6em1aaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaa"
        map_data = ascii2b64e(bytes(map(ord, map_data)))

        for i in range(0, len(map_data), 10):
            cell_data = map_data[i:i+10]
            self.assertEqual(cell_data, Tile.decode(cell_data).encode())

    def test_decode_empty_cell(self):
        cell_data = ascii2b64e(bytes(map(ord, "Hhaaeaaaaa")))
        cell = Tile.decode(cell_data)
        self.assertTrue(cell.active)
        self.assertTrue(cell.los)
        self.assertTrue(all(sprite.index == 0 for sprite in (cell.layers.ground, cell.layers.overlay, cell.layers.interactive)))
        self.assertEqual(Movement.NOT_WALKABLE, cell.mvt)

    def test_decode_ground_cell(self):
        cell_data = ascii2b64e(bytes(map(ord, "GhhceaaaWt")))
        cell = Tile.decode(cell_data)
        self.assertTrue(cell.active)
        self.assertFalse(cell.los)
        self.assertEqual(Movement.NOT_WALKABLE, cell.mvt)

        self.assertEqual(450, cell.layers.ground.index)
        self.assertFalse(cell.layers.ground.is_flipped)
        self.assertEqual(0, cell.layers.ground.rotation)
        self.assertEqual(7, cell.layers.ground.height)
        self.assertEqual(1, cell.layers.ground.slope)

        self.assertEqual(0, cell.layers.overlay.index)
        self.assertFalse(cell.layers.overlay.is_flipped)
        self.assertEqual(0, cell.layers.ground.rotation)

        self.assertEqual(3091, cell.layers.interactive.index)
        self.assertFalse(cell.layers.interactive.is_flipped)
        self.assertFalse(cell.layers.interactive.is_interactive)

    def test_decode_interactive_cell(self):
        cell_data = ascii2b64e(bytes(map(ord, "Ghiaeaad1F")))
        cell = Tile.decode(cell_data)
        self.assertTrue(cell.active)
        self.assertFalse(cell.los)
        self.assertEqual(Movement.NOT_WALKABLE_INTERACTIVE, cell.mvt)

        self.assertEqual(0, cell.layers.ground.index)
        self.assertFalse(cell.layers.ground.is_flipped)
        self.assertEqual(0, cell.layers.ground.rotation)
        self.assertEqual(7, cell.layers.ground.height)
        self.assertEqual(1, cell.layers.ground.slope)

        self.assertEqual(0, cell.layers.overlay.index)
        self.assertFalse(cell.layers.overlay.is_flipped)
        self.assertEqual(0, cell.layers.ground.rotation)

        self.assertEqual(7519, cell.layers.interactive.index)
        self.assertFalse(cell.layers.interactive.is_flipped)
        self.assertTrue(cell.layers.interactive.is_interactive)


class TestMap(unittest.TestCase):
    def test_decode_encode(self):
        # 2073_0706141524X.swf (Goball dungeon, room 1)
        map_data = "bhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaGhaaeaaatSHhaaeaaatPHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaetPHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes8aaaHhaaeaaaaaHhaaeaaaaaHhaaes8aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaaHhaaeaaaaaGhaaeaaatVHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhN2eaaaaaHhaaeaaaaaHhaaeaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhN2eaaaaaHhN_eaaaaaHhb6es-aaaHhN2eaaaqKHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhN2eaaaaaHhN_eaaaaaH3N9eaaaaaH3N7eaaaaaHhN2eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaGhaaeaaatSHhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHxN_eaaaaaHhN_eaaaaaHhH6eaNWaaHNN9eaaaaaHNN9eaaaaaHhN2eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhHfeaaaqvHNN9eaaaaaHhH6eaJWaaHhH6eaNaaaHhHfeaaaaaHxN_eaaaaaHhN2eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaabhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhHfeaaaaaHhH6eaNWaaHhH6eaJWaaHhH6eaJqaaHhGNeaaaaaHhHfem0aaaHxN_em0aaaH3N7eaaaaaHhN2eaaaaaHhaaes6aaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaes6iaaHhH6eaJaaaHhH6eaJWaaHhH6em0aaaHhH6eaJqaaHhHfeaaaaaHhHfeaaaaaHhH2eaNWaaHNN9eaaaaaHxN_eaaaaaHhN_etFaaaHhaaes6aaaHhaaeaaaaabhGaeaaaaaHhaaes6iaaHhH6etHiaaHhH6em1aaaHhH6eaaaaaHhH6eaaaaaHhHfeaaaaaHhH6eaNWaaHhH6eaJWaaHhH2eaJaaaHhH6eaNWaaHNN9eaaaaaHhGNetHaaaHhaaes6aaaHhaaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaJWaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaNWaaHhHfeaaaaaHhaaes6aaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6em0aaaHhH6eaaaaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaJWaaHhaaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaJqaaHhH6eaJGaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhaaetbaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhGNeaaaaaHhH6eaJGaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaNaaaHhHfeaaaaaHhH6eaaaaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhHfeaaaaaHhH6eaJWaaHhH6eaaaaaHhH6eaaaaaHhH6eaJqaaHhH6eaNqaaHhH6eaJGaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaJWaaHhH6eaaaaaHhH6eaaaaaHhH6eaJqaaHhGNeaaaaaHhHfeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6em1aaaHhH6eaaaaaHhH2eaaaaaHhHfeaaaaaHhHfeaaaaaHhH6eaNGaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaNaaaHhGNem0aaaHhHfeaJaqKHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH2eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhHfeaaaaaHhHfeaaaaaHhH6eaNGaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaGhb6eaaetLHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaJaaaHhH6eaNWaaHhH6eaNWaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaGhb6eaaetLHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6em1aaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaaHhH6eaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaabhGaeaaaaa"
        map_data = bytes(map(ord, map_data))
        content = Content.decode(map_data)

        self.assertEqual(map_data, Content.decode(map_data).encode())

    def test_mvt(self):
        data = "HhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaa6GHhaaeaaaaaHhaaeaaaaaHhaae6HaaaHhaae60aaaHhaaeaaaaaHhaae6HaaaHhaaeaaaaaGhaaeaaa7oHhaae6HiaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaa6SHhgSe6HaaaHhaaeaaa6IHhGaeaaaaaHhGaeaaaaaHhqaeaaaqgHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaa7iHhGaeaaaaaHhGaeaaa6IHhMSeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhWaeaaaaaHhGaeJgaaaHhGaeaaaaaGhaaeaaa7hHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaa6THhGaeaaaaaHhGaeaaaaaHhMSe62aaaHhGaeaaaaaHhGaeaaaaaHhGaeaaa6IHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhqaeaaaqgGhaaeaaa7AHhGaeaaaaaHhGaeaaaaaHhaae6Ha7eHhGaeaaaaaHhGaeaaaaaHhGaeaaa6IHhWaeaaaaaHhGaeaaaaaGhaaeaaa7gHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeJgaaaHhGaeaaaaaGhaaeaaa7jHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhWaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGae8uaaaGhaaeaaa7jHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGae8uaaaHhWae60aaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhqaeaaaqgHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeJgaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaa7iHhGaeJgaaaHhaaeaaaaaHhaaeJgaaaHhGae6HaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhWaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaa6IHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaa6IGhaaeaaa7hGhaaeaaa7iHhGaeaaaaaHhGaeaaaaaHhGaeJgaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaa7lGhaae8sa7gHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaGhaaeaaa7gGhaaeaaa7kHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhWae62aaaGhaaeaaa7kGhaaeaaa7hHhGaeaaaaaHhGaeaaaaaGhaaeaaa7lHhaaeaaaaaGhaaeaaa7nHhGaeaaaaaGhaaeaaa7lGhaaeaaa7jHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaa7hHhGaeaaaaaGhaaeaaa7mHhGaeaaaaaGhaaeJga7hHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhMTgJgaaaHhGaeaaaaaHhGaeaaa6IHhGaeaaaaaHhGaeaaaaaHhGae8saaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhMSeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaae6HaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaa7jHhGaeaaa6IHhGaeaaaaaHhaaeaaaaaHhaaeaaa6IHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaa7gHhGaeaaaaaHhGaeaaaaaHhaaeaaa6GHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaGhaaeaaa7kHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaa6GHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhgTeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaa7dHhaaeaaaaaHhaaeaaa6WHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaGhaaeaaa7yHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaa6XHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhMVgaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaa";
        data = bytes(map(ord, data))
        content = Content.decode(data)

        self.assertEqual(479, len(content))

        self.assertEqual(Movement.DEFAULT, content[230].mvt)

    def test_interactive(self):
        data = "HhaaeaaaaaHhaae6HaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHxaUeaaaaaHhaSeaaaaaHhaSeIWaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhGaeJgaaaHhGaeaaaaaHhqaeqgaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHxaWeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeIWaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhiTeaad1PHhaTeaaaaaHxaVedIaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSedIaaaHhaSeaaaaaHhaSeaaaaaHhGaeaaa6IHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHxaWeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSedIaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhiTeaad1PHhaTeaaaaaHxaVeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaadHHhaSeaaaaaHhGaeaaeaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHxaUeaaaaaHhaSeaaadHHhaSeaaaaaHhaSedIaaaHhaSeaaaaaHhaSeaaaaaHhaSeIWaaaHhaae6Ha7eHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHxaUeaaaaaHhaSeaaaaaHhaSedIaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaGhiaeaad1FHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhiTeaad1PHxaWeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeIWaaaHhaSeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhiTeaad1PHhaTeaaaaaHxaVeaaaaaHhaSez8aaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaadHHhaSeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaTeaaaaaHhaTeaaaaaHxaUeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaaeaaa6EHhGaeJgaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHxaWeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhiTeaad1PHhaTeaaaaaHhaUeaaaaaHhaSeaaaaaHhaSedIaaaHhaSeaaadHHhaSez8aaaHhaSeaaaaaHhaSeaaaaaHhaSedIaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHxaVeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhqaeaaaqgHhGaeaaa6IHhGaeaaaaaHhGaeaaaaaHhiTeaad1PHhaTeaaaaaHxaWeaaaaaHhaSeaaadHHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaadHHhaSeaaaaaHhaSeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhiTeaad1PHhaTeaaaaaHhaUeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSedIaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaHhGae6HaaaHhaaeaaa6UHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhiTeaad1PHhaTeaaaaaHhaUeaaaaaHhaVeaaaaaHhaVeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaadHHhaSeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaa6EHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHhaWez8aaaHhaWeaaaaaHhaUeaaaaaHhaSeaaaaaHhaSeaaaaaHhaSeaaaaaH3aUeaaaaaHhGaeaaaaaHhaaeaaa6YHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaUeaaaaaHhaVeaaaaaHhaVeaaadHH3aUeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaa6IHhGaeaaaaaGhaaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaWeaaaaaHhaWeaaaaaHhaWeaaaaaHhaTeaaaaaHhGaeJgaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaaaaGhaaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaaeaaaaaHhGaeaaaaaHhGae6HaaaHhGaeaaaaaGhaaeaaaaaGhaaeaaa8HHhGaeaaaaaHhiTeaad1PHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhiTeaad1PHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaGhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhiTeaad1PHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhaTeaaaaaHhiTeaad1PHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhiaeaad1PHhaTe8haaaHhaTeaaaaaHhiTeaad1PHhaaeaaaaaHhaaeaaaaaHhaaeaaa6IHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaTeaaaaaHhaTeaaaaaHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhiaeaad1PHhiaeaad1PHhaaeaaaaaHhGaeaaaaaHhaaeaaaaaGhaae6Ha6LHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaa7eHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeJgaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhqaeaaaqgHhaaeaaaaaHhaaeaaaaaHhaaeaaa6XHhGaeaaaaaHhGae6HaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhaaeaaaaaHhaae6Ha6FHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGae6HaaaHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaa6IHhGaeaaaaaHhGae8uaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeJgaaaHhGae8uaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaaaaHhGaeJgaaaHhGaeaaaaaHhqae8uaqgHhGaeaaaaaHhGaeJgaaaHhGaeaaaaaHhGaeaaaaaHhGaeaaa6IHhGaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaaaaHhaaeaaa6IGgaaeaaa7IGgaaeaaa7EHhaaeaaaaaGgaaeaaa7CGgaaeaaa7IHhaaeaaaaa"
        data = bytes(map(ord, data))
        content = Content.decode(data)

        self.assertEqual(479, len(content))
        cell = content[102]

        self.assertTrue(cell.active)
        self.assertFalse(cell.los)
        self.assertEqual(Movement.NOT_WALKABLE_INTERACTIVE, cell.mvt)

        self.assertEqual(0, cell.layers.ground.index)
        self.assertFalse(cell.layers.ground.is_flipped)
        self.assertEqual(0, cell.layers.ground.rotation)
        self.assertEqual(7, cell.layers.ground.height)
        self.assertEqual(1, cell.layers.ground.slope)

        self.assertEqual(0, cell.layers.overlay.index)
        self.assertFalse(cell.layers.overlay.is_flipped)
        self.assertEqual(0, cell.layers.ground.rotation)

        self.assertEqual(7519, cell.layers.interactive.index)
        self.assertFalse(cell.layers.interactive.is_flipped)
        self.assertTrue(cell.layers.interactive.is_interactive)

    def test_invalid_len(self):
        data = bytes(map(ord, "012345678"))
        self.assertRaises(ValueError, lambda: Content.decode(data))

    def test_invalid_char(self):
        data = bytes(map(ord, "##########"))
        self.assertRaises(ValueError, lambda: Content.decode(data))

