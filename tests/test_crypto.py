import unittest
from src.dofus_tools.Crypto import XORCipher, key_index_data_map, key_error_for_index, Analysis, data_block


class TestXorCipher(unittest.TestCase):
    def test_matching_input(self):
        data = bytes(map(ord, "ABCD"))
        XORCipher(data)(data)
        self.assertEqual(bytes((0, 0, 0, 0)), XORCipher(data)(data))

    def test_xor_zero(self):
        data = bytes(map(ord, "ABCD"))
        XORCipher(data)(data)
        self.assertEqual(data, XORCipher(data)(bytes((0, 0, 0, 0))))

    def test_xor_loop(self):
        data = bytes(map(ord, "ABCD"))
        key = bytes(map(ord, "EFGH"))
        cipher = XORCipher(key)
        self.assertEqual(data, cipher(cipher(data)))


class TestTools(unittest.TestCase):
    def test_key_index_data_extraction(self):
        data = bytes(map(ord, "".join("ABCD" for _ in range(10)) + "AB"))
        key_len = 4
        extracted_data_list = [key_index_data_map(data, i, key_len) for i in range(key_len)]
        for extracted_data, expected_value, expected_len in zip(extracted_data_list,
                                                                map(ord, ['A', 'B', 'C', 'D']),
                                                                (11, 11, 10, 10)):
            self.assertEqual(expected_len, len(extracted_data))
            data_set = set(extracted_data)
            self.assertEqual(1, len(data_set))
            self.assertEqual(expected_value, list(data_set)[0])

    def test_key_index_error_evaluation(self):
        analysis = Analysis(0.0,
                            {ord(c): 1.0 for c in "ABCD"},
                            [{ord(c): 1.0} for c in "ABCD"])

        in_data = bytes(map(ord, "".join("ABCD" for _ in range(10))))
        key_len = 3
        for i, d in enumerate(key_index_data_map(in_data, i, key_len) for i in range(key_len)):
            self.assertEqual(0, key_error_for_index(d, analysis.positional_frequencies, i, key_len, 10))

        in_data_err = [v if i not in (2, 5, 1) else ord('Z') for i, v in enumerate(in_data)]

        for i, (d, e) in enumerate(zip((key_index_data_map(in_data_err, i, key_len) for i in range(key_len)),
                                       (0.0, 10.0, 20.0))):
            self.assertEqual(e, key_error_for_index(d, analysis.positional_frequencies, i, key_len, 10))

    def test_key_block_extraction(self):
        data = bytes(map(ord, "".join("ABCD" for _ in range(10))))
        key_len = 3
        for i, e_s in enumerate(("ABC", "DAB", "CDA", "BCD")):
            self.assertEqual(bytes(map(ord, e_s)), data_block(data, i, key_len))



