import getopt
import json
import logging
import os
import sys
from pathlib import Path

from .Downloader import download_lang
from .Editor import convert_swf_dir

DEFAULT_OUTPUT_DIRNAME = "lang"

def usage(print_result=True, indent_level=0, indent_step=2):
    """
    Usage procedure for the Manual
    :param print_result: Whether to dump the manual to the console or not.
    :param indent_level: The root level of indentation of the manual.
    :param indent_step: The size of each indentation of the manual.
    :return: The string representation of the manual page.
    """

    res = ""
    indent = ' ' * indent_level * indent_step
    res += indent + "dofus_tools generate-langs [flags / options]\n"
    res += indent + "\n"
    res += indent + "Info: Generate the lang directory for the CDN of a Dofus server.\n"
    res += indent + "\n"
    res += indent + "Options:\n"
    re_indent = indent + ' ' * indent_step
    res += re_indent + "[-c / --config-dict=]    The path to the json configuration to inject in the lang files.\n"
    res += re_indent + "[-o / --output-path=]    The output-path, directory into which the lang directory is to be created (default=`%s/%s`).\n" % (os.getcwd(), DEFAULT_OUTPUT_DIRNAME)
    res += re_indent + "[-v / --verbose-level=]  Level of console logs amongst {critical, error, warning, info, debug, notset} (default=warning).\n"

    if print_result:
        print(res)

    return res


def main():
    """
    Lang Download / Edition tool entry-point.
    """
    try:
        opts, args = getopt.getopt(sys.argv[2:],
                                   "ho:c:v:",
                                   ["help", "output-path=", "config-dict=", "verbose-level="])
    except getopt.GetoptError as err:
        logging.exception(str(err))
        sys.exit(2)

    output = "/".join((os.getcwd(), DEFAULT_OUTPUT_DIRNAME))
    conf_path = None
    log_level = "WARN"
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-o", "--output-path"):
            output = a
        elif o in ("-c", "--config-dict"):
            conf_path = a
        elif o in ("-v", "--verbose-level"):
            log_level = a.upper()
        else:
            assert False, "unhandled option"

    logging.getLogger().setLevel(logging.getLevelName(log_level))

    registration_urls = None
    lobby_host = None
    lobby_port = None

    if conf_path is not None:
        with Path(conf_path).open("rb") as fi:
            params = json.loads(fi.read())

        if "registration_url" in params:
            registration_urls = params["registration_url"]
            if not isinstance(registration_urls, dict):
                raise ValueError("Unexpected config value type for 'registration_url': %s. Allowed type is 'dict'"
                                 % type(registration_urls))
            if "default" not in params["registration_url"]:
                raise ValueError("The 'registration_url' node of the configuration MUST define a 'default' child-node.")

        if "lobby_host" in params:
            lobby_host = params["lobby_host"]
            if not isinstance(lobby_host, str):
                raise ValueError("Unexpected config value type for 'lobby_host': %s. Allowed type is 'str'" % type(lobby_host))

        if "lobby_port" in params:
            lobby_port = params["lobby_port"]
            if not isinstance(lobby_port, int):
                raise ValueError("Unexpected config value type for 'lobby_port': %s. Allowed type is 'int'" % type(lobby_port))

    download_lang(output)

    conv_params = (registration_urls, lobby_host, lobby_port)
    if any(v is not None for v in conv_params):
        convert_swf_dir(output, *conv_params)


if __name__ == "__main__":
    main()