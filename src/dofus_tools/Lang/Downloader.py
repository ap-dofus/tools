from typing import Set
import logging
import os
import requests
from pathlib import Path


def download_file_content(url: str) -> bytes:
    """
    Download file from the web.
    :param url: The url location of the file to download.
    :return: The file content.
    """
    return requests.get(url).content


ANKAMA_CDN = "https://dofusretro.cdn.ankama.com"
LANG_PATH = "/".join((ANKAMA_CDN, "lang"))
SWF_PATH = "/".join((LANG_PATH, "swf"))
VERSION_FILE_TEMPLATE = "versions_%s.txt"
SWF_FILE_TEMPLATE = "%s_%s_%s.swf"

AVAILABLE_LOCALES = {"de", "en", "es", "fr", "it", "nl", "pt"}


def download_lang(output_path: str="lang", locales: Set[str]=None):
    """
    Download the lang directory of the Ankama CDN.
    :param output_path: The path to download the lang directory to.
    :param locales: Set of locales to download files for.
    """
    if not os.path.exists(output_path):
        # Create a new directory because it does not exist
        os.makedirs(output_path)
    if not os.path.exists("/".join((output_path, "swf"))):
        # Create a new directory because it does not exist
        os.makedirs("/".join((output_path, "swf")))

    if locales is None:
        locales = AVAILABLE_LOCALES

    with Path(output_path, "versions.swf").open("wb") as fo:
        fo.write(download_file_content("/".join((LANG_PATH, "versions.swf"))))

    for l in locales:
        version_file_name = VERSION_FILE_TEMPLATE % l
        logging.info("Downloading: %s" % version_file_name)
        version_file_str_content = "".join(map(chr, download_file_content("/".join((LANG_PATH, version_file_name)))))
        if not version_file_str_content.startswith("&f="):
            raise ValueError("Unexpected format for %s" % version_file_name)
        version_file_str_content = version_file_str_content[3:]
        with Path(output_path, version_file_name).open("w") as fo:
            fo.write(version_file_str_content)
        for f_p, f_l, f_si in (ff.split(',') for ff in version_file_str_content.split('|') if ff != ""):
            swf_file_name = SWF_FILE_TEMPLATE % (f_p, f_l, f_si)
            logging.info("Downloading: %s" % swf_file_name)
            with Path(output_path, "swf", swf_file_name).open("wb") as fo:
                fo.write(download_file_content("/".join((SWF_PATH, swf_file_name))))
