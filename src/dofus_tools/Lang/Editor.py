from typing import Dict
import logging
import tempfile
import os
from ..Tools import load_xml_encoded_swf

LANG_REG_KEY = (5, "REGISTER_POPUP_LINK", 0)
LANG_HOST_KEY = (5, "SERVER_NAME", 0)
LANG_PORT_KEY = (5, "SERVER_PORT", 1)

path = 'lang/swf/'
tmp = 'tmp/'


UNPACK_SWF_CMD_TEMPLATE = "swfmill swf2xml %s %s"
PACK_SWF_CMD_TEMPLATE = "swfmill xml2swf %s %s"

CHANGE_SWF_CMD_TEMPLATE = "sed -i -e 's/%s/%s/g' %s"

ANKAMA_ACCOUNT_CREATION_LINKS = {
  "en": "https://www.dofus.com/en/account-creation",
  "es": "https://www.dofus.com/es/creacion-de-cuenta",
  "fr": "https://www.dofus.com/fr/creer-un-compte",
  "it": "https://www.dofus.com/it/crea-un-account",
  "nl": "https://www.dofus.com/en/account-creation",
}


def _escape(s: str) -> str:
    """
    Escapes slashes in strings.
    :param s: The string in which to escape slashes.
    :return: The param s, with the slashes escaped.
    """
    return s.replace('/', "\/")


def convert_lang_swf(file_path: str, cache_dir: str, reg_urls: Dict[str, str]=None,
                     conn_host: str=None, conn_port: int=None):
    """
    Convert a specific lang type swf to match a specified server config
    :param file_path: Path to the lang type swf file to convert.
    :param cache_dir: Directory to use for temporary file.
    :param reg_urls: Dictionary containing registration urls for each lang (the "default" lang is mandatory).
    :param conn_host: Ip, URL, or hostname, pointing to the host of the lobby server.
    :param conn_port: Port on which the lobby server is listening for new connections.
    """
    file_name = os.path.basename(file_path)
    if not file_name.startswith("lang_"):
        raise ValueError("File %s is not a lang swf file")
    locale = file_name.split('_')[1]

    cache_file_name = '.'.join((file_name.rsplit('.')[0], "xml"))
    cache_file_path = '/'.join((cache_dir, cache_file_name))
    os.system(UNPACK_SWF_CMD_TEMPLATE % (file_path, cache_file_path))

    base_content = load_xml_encoded_swf(cache_file_path)

    if reg_urls is not None:
        if locale in reg_urls:
            reg_url = reg_urls[locale]
        else:
            reg_url = reg_urls["default"]
        base_reg_host = base_content[LANG_REG_KEY[0]][LANG_REG_KEY[1]][LANG_REG_KEY[2]]
        REG_TEMPLATE = '<String value="%s"/>'
        os.system(CHANGE_SWF_CMD_TEMPLATE % (_escape(REG_TEMPLATE % base_reg_host),
                                             _escape(REG_TEMPLATE % reg_url),
                                             cache_file_path))

    if conn_host is not None:
        base_conn_host = base_content[LANG_HOST_KEY[0]][LANG_HOST_KEY[1]][LANG_HOST_KEY[2]]
        REG_TEMPLATE = '<String value="%s"/>'
        os.system(CHANGE_SWF_CMD_TEMPLATE % (_escape(REG_TEMPLATE % base_conn_host),
                                             _escape(REG_TEMPLATE % conn_host),
                                             cache_file_path))

    if conn_port is not None:
        base_conn_port = int(base_content[LANG_PORT_KEY[0]][LANG_PORT_KEY[1]][LANG_PORT_KEY[2]])
        print(base_conn_port)
        REG_TEMPLATE = '<StackInteger value="%i"/>'
        os.system(CHANGE_SWF_CMD_TEMPLATE % (_escape(REG_TEMPLATE % base_conn_port),
                                             _escape(REG_TEMPLATE % conn_port),
                                             cache_file_path))

    os.system(PACK_SWF_CMD_TEMPLATE % (cache_file_path, file_path))


def convert_swf_dir(path: str, reg_urls: Dict[str, str]=None, conn_host: str="localhost",
                    conn_port: int=None, cache_dir: str=None):
    """
    Convert the swf directory to match a specified server config
    :param path: Path to the swf directory to convert.
    :param reg_urls: Dictionary containing registration urls for each lang (the "default" lang is mandatory).
    :param conn_host: Ip, URL, or hostname, pointing to the host of the lobby server.
    :param conn_port: Port on which the lobby server is listening for new connections.
    :param cache_dir: Directory to use for temporary file.
    """
    if cache_dir is None:
        with tempfile.TemporaryDirectory() as tmp_cache_dir:
            return convert_swf_dir(path, reg_urls, conn_host, conn_port, tmp_cache_dir)

    for r, d, files in os.walk(path):
        for file in files:
            if "lang_" in file:
                logging.info("Editing: %s" % file)
                convert_lang_swf("/".join((r, file)), cache_dir, reg_urls, conn_host, conn_port)
