import sys
from dofus_tools.Maps.ConvertMain import main as maps_convert_entrypoint
from dofus_tools.Maps.ConvertMain import usage as maps_convert_usage
from dofus_tools.Maps.DisplayMain import main as maps_display_entrypoint
from dofus_tools.Maps.DisplayMain import usage as maps_display_usage
from dofus_tools.Lang.GenerateMain import usage as lang_generation_usage
from dofus_tools.Lang.GenerateMain import main as lang_generation_entrypoint


def usage(print_result=True, indent_level=0, indent_step=2):
    """
    Usage procedure for the Manual
    :param print_result: Whether to dump the manual to the console or not.
    :param indent_level: The root level of indentation of the manual.
    :param indent_step: The size of each indentation of the manual.
    :return: The string representation of the manual page.
    """
    res = ""
    indent = ' ' * indent_level * indent_step
    res += indent + "dofus_tools <command> ...\n"
    res += indent + "\n"
    res += indent + "Info: Tools for the data associated to the Dofus game.\n"
    res += indent + '=' * 50 + '\n'
    res += maps_convert_usage(False, indent_level + 1, indent_step)
    res += indent + '=' * 50 + '\n'
    res += maps_display_usage(False, indent_level + 1, indent_step)
    res += indent + '=' * 50 + '\n'
    res += lang_generation_usage(False, indent_level + 1, indent_step)
    res += indent + '=' * 50 + '\n'

    if print_result:
        print(res)

    return res


def main():
    """
    dofus_tools program entry-point.
    """
    if len(sys.argv) < 2:
        print("No command specified.")
        usage()
    elif sys.argv[1].lower() in ("map-convert", "maps-convert"):
        maps_convert_entrypoint()
    elif sys.argv[1].lower() in ("map-display", "maps-display"):
        maps_display_entrypoint()
    elif sys.argv[1].lower() in ("generate-lang", "generate-langs"):
        lang_generation_entrypoint()
    elif sys.argv[1].lower() in ("-h", "--help"):
        usage()
    else:
        raise ValueError("Unknown command: %s" % sys.argv[1])


if __name__ == "__main__":
    main()
