def xor_int(a: int, b: int) -> int:
    """
    Hamming distance (number of steps) according to the bitwise XOR operator between two bytes.

    :param a: Byte to measure distance from.
    :param b: Byte to measure distance to.
    :return: The Hamming distance (number of steps) according to the bitwise XOR operator between a and b.
    """
    dist = 0
    x = a ^ b
    while x != 0:
        dist += 1
        x &= x - 1

    return dist


def xor_bytes(a: bytes, b: bytes) -> int:
    """
    Hamming distance (number of steps) according to the bitwise XOR operator between two byte arrays.

    :param a: Byte array to measure distance from.
    :param b: Byte array to measure distance to.
    :return: The Hamming distance (number of steps) according to the bitwise XOR operator between a and b.
    """
    return sum(xor_int(*c_t) for c_t in zip(a, b))
