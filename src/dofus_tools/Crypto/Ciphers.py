from itertools import cycle


class XOR(object):
    """
    Tool for ciphering / deciphering data by applying the XOR oprerator with a cycled key.
    """
    def __init__(self, key: bytes):
        """
        Constructor
        :param key: The key to cipher / decipher with
        """
        self.key = key

    def __call__(self, data: bytes):
        """
        Ciphers / Deciphers the data.

        :param data: Ciphered or clear data.
        :return: Ciphered data if the parameter was clear data. Clear data if the parameter was ciphered data.
        """
        return bytes(d_c ^ k_c for d_c, k_c in zip(data, cycle(self.key)))
