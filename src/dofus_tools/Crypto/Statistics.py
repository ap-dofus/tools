import json
from typing import List, Dict, Set
from ..Tools import coincidence_rate


class DataFrequencies(Dict[int, float]):
    """
    Dictionary of absolute frequency of any byte in a given dataset
    """
    pass


class PositionalDataFrequencies(List[Dict[int, float]]):
    """
    Dictionary of the frequency of any byte in a given dataset for each position of a sequence of fixed length.
    """
    pass


class Analysis(object):
    """
    Absolute and positional frequency analysis of a dataset containing sequences of fixed length.
    """
    def __init__(self, coincidence: float,
                 global_frequencies: DataFrequencies,
                 positional_frequencies: PositionalDataFrequencies):
        """
        Constructor
        :param coincidence: Average coincidence-rate in the dataset.
        :param global_frequencies: Absolute frequency analysis instance associated to the dataset.
        :param positional_frequencies: Positional frequency analysis instance associated to the dataset.
        """

        self.coincidence = coincidence
        self.global_frequencies = global_frequencies
        self.positional_frequencies = positional_frequencies

    def encode(self):
        """
        Encodes the Analysis instance to a string.
        :return: The string encoded representation of the analysis.
        """
        def k2c(d):
            return {chr(k): f for k, f in d.items()}
        return json.dumps({
            "coincidence": self.coincidence,
            "frequencies": {
                "global": k2c(self.global_frequencies),
                "positional": list(map(k2c, self.positional_frequencies))
            }
        }, sort_keys=True, indent=2, ensure_ascii=False)

    @staticmethod
    def decode(data: str):
        """
        Decodes an Analysis instance from a string.
        :return: The Analysis instance decoded from the string.
        """
        def k2i(d):
            return {ord(k): v for k, v in d.items()}

        raw = json.loads(data)
        return Analysis(raw["coincidence"],
                        DataFrequencies(k2i(raw["frequencies"]["global"])),
                        PositionalDataFrequencies([k2i(d) for d in raw["frequencies"]["positional"]]))

    @staticmethod
    def run(dataset: List[bytes], pattern_len=10):
        """
        Run a statistical analysis on a dataset.
        :param dataset: The dataset to run analysis on.
        :param pattern_len: The size of the type of element contained in the datas of the dataset.
        :return: The Analysis resulting from the dataset.
        """
        coincidence = 0
        global_stats = dict()
        pos_stats = [dict() for _ in range(pattern_len)]
        data_count = 0
        byte_count = 0
        pos_stats_count = [0 for _ in range(pattern_len)]

        for data in dataset:
            coincidence += coincidence_rate(data)
            for b_i, b in enumerate(data):
                if b not in global_stats:
                    global_stats[b] = 1
                else:
                    global_stats[b] += 1

                if b not in pos_stats[b_i % pattern_len]:
                    pos_stats[b_i % pattern_len][b] = 1
                else:
                    pos_stats[b_i % pattern_len][b] += 1

                byte_count += 1
                pos_stats_count[b_i % pattern_len] += 1
            data_count += 1

        return Analysis(coincidence / data_count,
                        DataFrequencies({k: v / byte_count for k, v in global_stats.items()}),
                        PositionalDataFrequencies([{k: min(v / p_s_c, 0.99999999999) for k, v in p_s.items()}
                                                   for p_s, p_s_c in zip(pos_stats, pos_stats_count)]))
