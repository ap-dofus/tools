import math

from .Statistics import PositionalDataFrequencies


def key_index_data_map(data: bytes, key_index: int, key_len: int) -> bytes:
    """
    Maps the data subset that is meant to be ciphered / deciphered by the key byte at a specific key index.
    :param data: The full data.
    :param key_index: The index of key that is supposed to cipher / decipher the mapped data?
    :param key_len: The length of the key.
    :return: The subset of data that is meant to be ciphered / deciphered by the key byte at key_index
    """
    return bytes(data[i] for i in range(key_index, len(data), key_len))


def key_error_for_index(deciphered_mapped_data: bytes, pos_stats: PositionalDataFrequencies,
                        key_i: int, key_len: int, unknown_error: float = 10.0) -> float:
    """
    Computes the probabilistic error corresponding to data subset that was deciphered by the key byte at a specific key index.

    :param deciphered_mapped_data: Data subset that was deciphered by the key byte at a specific key index
    :param pos_stats: Position-based statistical analysis of a representative dataset.
    :param key_i: The index of key that handled deciphering for this data subset
    :param key_len: The length of the key.
    :param unknown_error: The probabilistic error associated to a byte that does not appear in the statistical analysis.
    :return: the probabilistic error corresponding to the deciphered_mapped_data that was deciphered by the key byte at key_i.
    """
    return sum(
        ((1.0 - pos_stat[v]) if v in pos_stat else unknown_error)
        for pos_stat, v in ((pos_stats[(i * key_len + key_i) % len(pos_stats)], v)
                            for i, v in enumerate(deciphered_mapped_data))
    )


def data_block(data: bytes, block_index: int, key_len: int) -> bytes:
    """
    The data subset to be deciphered by a complete key, after a certain amount of key-cycling.
    :param data: The full data.
    :param block_index: The amount of key cycling before the subset to extract.
    :param key_len: The length of the key.
    :return: The subset of data to be deciphered by a complete key after a block_index key-cycling.
    """
    d_i = block_index * key_len
    if d_i >= len(data):
        raise KeyError("Block %i not in data. The length of data_blocks for data of size %u and a key of length %u is %u"
                       % (block_index, len(data), key_len, int(math.ceil(float(len(data)) / min(len(data), key_len)))))
    return data[d_i:min(d_i + key_len, len(data))]
