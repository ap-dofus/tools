import logging
import os
import json
from pathlib import Path
from typing import Union

from .Model import Raw, DecipheringOutput


def import_map(path: str) -> Union[Raw, DecipheringOutput]:
    with Path(path).open('rb') as fi:
        map_data = json.load(fi)
        if 'solutions' in map_data:
            return DecipheringOutput.from_json(map_data)
        else:
            return Raw.from_json(map_data)