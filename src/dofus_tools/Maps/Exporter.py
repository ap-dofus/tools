import logging
import os
import json
from pathlib import Path


class Exporter(object):
    """
    Tool to export maps encoded as dictionaries to .json files.
    """
    def __init__(self, path: str):
        """
        Constructor.
        :param path: The path to the directory to export maps into.
        """
        self.path = path

    def export(self, d: dict):
        """
        Exports the dictionary encoded map.
        :param d: The dictionary encoded map.
        """
        logging.info("Exporting map data to '%s/%s_%sX.json'" % (self.path, d["id"], d["date_tag"]))
        with Path(self.path, "%s_%sX.json" % (d["id"], d["date_tag"])).open('w') as fo:
            fo.write(json.dumps(d, indent=2))

    def __enter__(self):
        """
        Scope entrance operator. Will attempt to create the output directory if it doesn't exist.
        """
        if not Path(self.path).exists():
            logging.info("Creating output directory path '%s'" % self.path)
            os.makedirs(self.path)

        if not Path(self.path).is_dir():
            raise ValueError("Exporter path \"%s\" is not a directory." % self.path)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Scope exit operator
        """
        pass
