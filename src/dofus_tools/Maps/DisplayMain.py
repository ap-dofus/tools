import getopt
import logging
from pathlib import Path

from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
from pygame.locals import QUIT, KEYDOWN, K_ESCAPE

import pygame, sys, time
from .Model import Decoded, Raw, DecipheringOutput
from .Views import Content as ContentView
from .Importer import import_map
from .Key import Key


def usage(print_result=True, indent_level=0, indent_step=2):
    """
    Usage procedure for the Manual
    :param print_result: Whether to dump the manual to the console or not.
    :param indent_level: The root level of indentation of the manual.
    :param indent_step: The size of each indentation of the manual.
    :return: The string representation of the manual page.
    """

    res = ""
    indent = ' ' * indent_level * indent_step
    res += indent + "dofus_tools maps-display [options] <extracted_map_file>.json\n"
    res += indent + "\n"
    res += indent + "Info: Sketch display for extracted maps of the Dofus game.\n"
    res += indent + "\n"
    res += indent + "Options:\n"
    re_indent = indent + ' ' * indent_step
    res += re_indent + "[-n / --key-number=]     Index of the key to use (default=0). Only available if the input file contains key predictions.\n"
    res += re_indent + "[-k / --key=]            Hex encoded key\n"

    if print_result:
        print(res)

    return res


class MapDisplay(object):
    """
    Tool to display the sketch of a map from the Dofus game through the pygame API
    """
    def __init__(self, decoded_map: Decoded):
        """
        Constructor
        :param decoded: The deciphered & decoded map of the dofus game to sketch.
        """
        pygame.init()

        self.map = decoded_map
        self.content_view = ContentView(self.map.coordinates_system)

        pygame.display.set_caption('Dofus map %i_%sX' % (self.map.index, self.map.date_tag))
        self.screen = pygame.display.set_mode(tuple(50 * c for c in self.map.coordinates_system.map_dimensions), 0, 32)
        self.display = pygame.Surface(self.content_view.size)

    def run(self):
        """
        Window drawing & event-handling routine.
        """
        while True:
            self.display.fill((0, 0, 0))
            self.content_view.blit(self.display, self.map.content)

            for event in pygame.event.get():
                if any((event.type == QUIT,
                        event.type == KEYDOWN and event.key == K_ESCAPE)):
                    pygame.quit()
                    return

            self.screen.blit(pygame.transform.scale(self.display, self.screen.get_size()), (0, 0))
            pygame.display.update()
            time.sleep(1)


def main():
    """
    Maps Display tool entry-point.
    """
    try:
        opts, args = getopt.getopt(sys.argv[2:], "hk:n:", ["help", "key=", "key-number="])
    except getopt.GetoptError as err:
        logging.exception(str(err))
        sys.exit(2)

    key_index = None
    key = None
    for o, a in opts:
        if o in ("-h", "--help"):
            usage(print_result=True)
            sys.exit()
        if o in ("-k", "--key"):
            key = a
        if o in ("-n", "--key-number"):
            key_index = a
        else:
            assert False, "unhandled option"

    if key_index is not None and key is not None:
        logging.exception("Only one of {[-n / --key-number], [-k / --key]} can be set, both currently are. Use [-h / --help] for help")
        sys.exit(2)

    if len(args) < 1:
        logging.exception("Missing positional input-file parameter. Use [-h / --help] for help")
        sys.exit(2)

    input = args[0]
    try:
        with Path(args[0]).open('rb') as fi:
            data = import_map(args[0])
    except Exception as e:
        logging.exception(e)
        sys.exit(2)

    if isinstance(data, Raw):
        if key is not None:
            key = Key.decode(key)
        else:
            if key_index is not None:
                logging.exception("[-n / --key-number]} was provided, but the loaded map did not contain any candidate key.")
            else:
                logging.exception("No key provided. Use [-h / --help]")
            sys.exit(2)

    elif isinstance(data, DecipheringOutput):
        if key is not None:
            key = Key.decode(key)
        else:
            key = data.deciphered_options[int(key_index) if key_index is not None else 0].key

        data = data.raw_map

    MapDisplay(Decoded(data, key)).run()


if __name__ == "__main__":
    main()
