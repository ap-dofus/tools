from typing import Tuple
from cmath import sqrt
from pygame import Surface, Rect, transform, draw
from pygame.constants import SRCALPHA
from ..Model.Content import Tile as TileModel



class Tile(object):
    def __init__(self, cell_size: int=10, cell_border: int=1):

        self.width = int(2 * sqrt(cell_size ** 2).real)
        self.base_sprite = Surface((cell_size, cell_size), SRCALPHA)
        self.base_sprite.fill("black")
        self.base_sprite = transform.rotate(self.base_sprite, 45)

        self.walkable_sprite = Surface((cell_size, cell_size), SRCALPHA)
        draw.rect(self.walkable_sprite,
                  (255, 255, 255),
                  Rect(cell_border, cell_border,
                       cell_size - 2 * cell_border, cell_size - 2 * cell_border), 0)
        self.walkable_sprite = transform.rotate(self.walkable_sprite, 45)

        self.no_los_sprite = Surface((cell_size, cell_size), SRCALPHA)
        draw.line(self.no_los_sprite, "red", (cell_border, cell_size / 2), (cell_size - 1 - cell_border, cell_size / 2))
        draw.line(self.no_los_sprite, "red", (cell_size // 2, cell_border), (cell_size // 2, cell_size - 1 - cell_border))
        draw.line(self.no_los_sprite, "red", (cell_border, cell_size / 2 - 1), (cell_size - 1 - cell_border, cell_size / 2 - 1))
        draw.line(self.no_los_sprite, "red", (cell_size // 2 - 1, cell_border), (cell_size // 2 - 1, cell_size - 1 - cell_border))
        self.no_los_sprite = transform.rotate(self.no_los_sprite, 45)

    def blit(self, surf: Surface, tile: TileModel, pos: Tuple[int, int]):
        """
        Draws tile on surface.
        :param surf: The surface on which to draw the tile.
        :param tile: The tile to draw.
        :param pos: The position at which to draw the tile on the surface.
        """
        surf.blit(self.base_sprite, pos)
        if tile.walkable():
            surf.blit(self.walkable_sprite, pos)
        if not tile.los:
            surf.blit(self.no_los_sprite, pos)
