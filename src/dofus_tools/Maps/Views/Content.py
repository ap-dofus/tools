from typing import Tuple
from pygame import Surface
from ..Model.Content import Content as ContentModel
from ..Model.Content import CoordinatesSystem
from .Tile import Tile


class Content(object):
    """
    View for map content.
    """
    def __init__(self, map_coordinates: CoordinatesSystem):
        """
        Constructor
        :param map_coordinates: The coordinate system of the map.
        """
        self.tile_view = Tile()
        self.coord_system = map_coordinates
        self.size = tuple(self.tile_view.width * d for d in map_coordinates.map_dimensions)
        self.coord_transform = [[v * self.tile_view.width // 2 for v in r]
                                for r in [[1, -1],
                                          [1,  1]]]

    def blit(self, surf: Surface, content: ContentModel, pos: Tuple[int, int] = (0, 0)):
        """
        Draws map content on surface.
        :param surf: The surface on which to draw the map.
        :param content: The map content to draw.
        :param pos: The position at which to draw the map content on the surface.
        """
        def mat_mul(x, y):
            return [[sum(a * b for a, b in zip(x_row, y_col)) for y_col in zip(*y)] for x_row in x]

        for (x, y), c in zip(self.coord_system.coordinates(), map(content.__getitem__, self.coord_system.indices())):
            self.tile_view.blit(surf, c,
                                tuple(sum(t) for t in zip(pos,
                                                          tuple(zip(*mat_mul(self.coord_transform,
                                                                             list(zip(*[(x, y)])))))[0])))
