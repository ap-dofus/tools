import logging
import os
import tempfile
from typing import Iterator
import xml.etree.ElementTree as ElementTree

from ..Tools import load_xml_encoded_swf
from .Model import Raw, Data


def _swf2xml(input_path: str, output_path: str):
    """
    Copy the data contained in a .swf (Shockwave Flash) file to a .xml file.
    :param input_path: The path of the .swf file to copy data from.
    :param output_path: The path of the output .xml file to copy data into.
    """
    os.system("swfmill swf2xml %s %s" % (input_path, output_path))


def load_swf(input_path: str, suffix_len=1, decode: bool = True) -> Raw:
    """
    Loads the raw dofus map data from a .swf file

    :param input_path: The path of the .swf file to load data from.
    :param suffix_len: Length of the date-tag suffix.
    :param decode: Whether to decode the ciphered data or not.
    :return: The model instance of Raw map parsed from the file.
    """
    logging.debug("Loading map data from %s" % input_path)
    with tempfile.TemporaryDirectory() as tmpDir:
        m_id, day = input_path.rsplit("/")[-1].rsplit(".")[0].rsplit("_")
        m_id, day = int(m_id), (day[:-suffix_len] if suffix_len != 0 else day)

        xml_path = "%s/%s.xml" % (tmpDir, input_path.rsplit("/")[-1].rsplit(".")[0])
        _swf2xml("%s" % (input_path), xml_path)
        data = load_xml_encoded_swf(xml_path, "SetVariable")[0]
        data = {k: t[0] for k, t in data.items()}
        if m_id != data["id"]:
            raise ValueError("The id (%i) read in file %s is inconsistent with the filename."
                             % (m_id, input_path.rsplit("/")[-1]))
        return Raw(m_id, day, (data["width"], data["height"]), data["backgroundNum"],
                   data["ambianceId"], data["musicId"], data["bOutdoor"], data["capabilities"],
                   Data.decode(data["mapData"]) if decode else Data(map(ord, data["mapData"])))


class SWFExplorer(object):
    """
    Tool to explore .swf maps contained in a directory.
    """
    def __init__(self, path):
        """
        Constructor.
        :param path: Path of the directory to search .swf in.
        """
        self.path = path

    def __iter__(self) -> Iterator[Raw]:
        """
        Iteration operator.
        :return: A generator of model instance of Raw map (one for each .swf file in the directory).
        """
        for ffn in os.listdir(self.path):
            if len(ffn.rsplit(".")) < 2:
                continue

            fn, ext = ffn.rsplit(".")
            if ext == "swf":
                full_path = "%s/%s" % (self.path, ffn)
                try:
                    yield load_swf(full_path)
                except FileNotFoundError:
                    pass
