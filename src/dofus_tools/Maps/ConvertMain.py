from typing import Union
import getopt
import logging
import os
import sys
from pathlib import Path
from multiprocessing import Pool
from functools import partial

from ..Tools import chain
from ..Crypto import Analysis as StatisticalDataAnalysis
from .Exporter import Exporter
from .Key import Explorer as KeyExplorer
from .Key import LengthsPredictor as KeyLengthPredictor
from .Model import DecipheringOutput, DecipheredData, DecipheringOptions, Raw
from .Parsing import SWFExplorer, load_swf

DEFAULT_KEY_COUNT = 10
DEFAULT_THREADS_COUNT = 1
DEFAULT_OUTPUT_DIRNAME = "output"
STATS_FILE_NAME = "data-statistics.json"


def usage(print_result=True, indent_level=0, indent_step=2):
    """
    Usage procedure for the Manual
    :param print_result: Whether to dump the manual to the console or not.
    :param indent_level: The root level of indentation of the manual.
    :param indent_step: The size of each indentation of the manual.
    :return: The string representation of the manual page.
    """

    res = ""
    indent = ' ' * indent_level * indent_step
    res += indent + "dofus_tools maps-convert [flags / options] <input_(swf-file/dir)_path>\n"
    res += indent + "\n"
    res += indent + "Info: Key crack, and data extractor for maps of the Dofus game.\n"
    res += indent + "\n"
    res += indent + "Flags:\n"
    re_indent = indent + ' ' * indent_step
    res += re_indent + "[-k / --key-crack]         Whether to attempt to crack the keys for the extracted maps.\n"
    res += re_indent + "[-e / --exhaustive-length] Whether to test all possible key length when the conventional prediction methods failed.\n"
    res += indent + "\n"
    res += indent + "Options:\n"
    res += re_indent + "[-v / --verbose-level=]  Level of console logs amongst {critical, error, warning, info, debug, notset} (default=warning).\n"
    res += re_indent + "[-f / --frequency-file=] File containing a statistical analysis of the data to be deciphered (A default analysis, distributed with the package, will be used if none is provided).\n"
    res += re_indent + "[-o / --output-path=]    The output-path, directory into which the data is to be exported (default=`%s/%s`).\n" % (os.getcwd(), DEFAULT_OUTPUT_DIRNAME)
    res += re_indent + "[-m / --max-keys=]       The maximum amount of keys to be generated per map (default=%i).\n" % DEFAULT_KEY_COUNT
    res += re_indent + "[-w / --worker-count=]   The amount of threads allocated to the workflow (default=%i).\n" % DEFAULT_THREADS_COUNT

    if print_result:
        print(res)

    return res


def key_crack_workflow(rm: Raw, stats: StatisticalDataAnalysis, max_keys: int=10, exhaustive_key_length: bool=False) \
        -> Union[DecipheringOutput, Raw]:
    """
    Attempts to generate keys for the ciphered map-content contained in a specific raw map data.
    :param rm: The raw map data containing the ciphered map-content to generate keys for.
    :param stats: The whole (absolute + positional) statistical analysis of a representative dataset.
    :param max_keys: The maximum amount of keys to generate.
    :param exhaustive_key_length: Whether to try all keys length when conventional length prediction methods failed.
    :return: The instance of Deciphered model if any valid candidate key was found. The same Raw instance otherwise.
    """
    logging.info(f"Generating keys for the map '%i_%sX.swf'." % (rm.index, rm.date_tag))
    key_length_predictor = KeyLengthPredictor()
    explorer = KeyExplorer(stats.positional_frequencies)
    deciphered = DecipheringOptions()
    for k_l in key_length_predictor.possible_key_length(rm.ciphered_data, exhaustive=exhaustive_key_length):
        deciphered_len = -len(deciphered)
        for k, e in explorer.explore_keys(rm.ciphered_data, k_l)\
                            .bytewise_prune(max_keys)\
                            .prune(max_keys * 2):
            clear_data = k(rm.ciphered_data)
            if not clear_data.is_invalid(stats.global_frequencies):
                conf = 1.0 / (e+1)
                logging.debug("possible key of len %u found for the map '%i_%sX.swf' (confidence: %5f): '%s'." % (k_l, rm.index, rm.date_tag, conf, k.encode()))
                deciphered.append(DecipheredData(k, clear_data, conf))

            if len(deciphered) >= max_keys:
                break

        if len(deciphered) + deciphered_len == 0:
            logging.debug("No key of len %u found for the map '%i_%sX.swf'." % (k_l, rm.index, rm.date_tag))
        if len(deciphered) >= max_keys:
            break

    if len(deciphered) > 0:
        return DecipheringOutput(rm, deciphered)
    else:
        logging.warning("Failed to generate keys for the map '%i_%sX.swf'" % (rm.index, rm.date_tag))
        return rm


def map_data_to_json(m_d: Union[Raw, DecipheringOutput]) -> dict:
    """
    Converts some map data to a dictionary that is can be json-encoded itself.
    :param m_d: The map data to encode.
    :return: A dictionary, representing the map data, that is can be json-encoded
    """
    return m_d.to_json()


def main():
    """
    Maps conversion / key-crack tool entry-point.
    """
    try:
        opts, args = getopt.getopt(sys.argv[2:],
                                   "hkei:o:m:f:v:w:",
                                   ["help", "key-crack", "exhaustive-length", "output-path=", "max-keys=",
                                    "frequency-file=", "verbose-level=", "worker-count="])
    except getopt.GetoptError as err:
        logging.exception(str(err))
        sys.exit(2)

    output = "/".join((os.getcwd(), DEFAULT_OUTPUT_DIRNAME))
    k_crack = False
    e_len = False
    m_k = None
    f_s = None
    worker_count = None
    log_level = "WARN"
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-k", "--key-crack"):
            k_crack = True
        elif o in ("-e", "--exhaustive-length"):
            e_len = True
        elif o in ("-o", "--output-path"):
            output = a
        elif o in ("-m", "--max-keys"):
            m_k = int(a)
        elif o in ("-f", "--frequency-file"):
            f_s = a
        elif o in ("-v", "--verbose-level"):
            log_level = a.upper()
        elif o in ("-w", "--worker-count"):
            worker_count = int(a)
        else:
            assert False, "unhandled option"
    logging.getLogger().setLevel(logging.getLevelName(log_level))

    if len(args) < 1:
        print("Missing required positional argument <input_(swf-file/dir)_path>. See `dofus_tools maps -h` for help.")
        sys.exit(2)

    input = args[0]

    if k_crack:
        if f_s is not None:
            logging.info("Loading statistical analysis from %s" % f_s)
            if not Path(f_s).exists():
                raise ValueError("Parametric frequency-file \"%s\" not found." % f_s)
            if not Path(f_s).is_file():
                raise ValueError("Parametric frequency-file path \"%s\" is not a file." % f_s)
            with Path(f_s).open("rb") as fi:
                stats = StatisticalDataAnalysis.decode(fi.read())
        else:
            search_result = list(filter(os.path.isfile,
                                        map(lambda dir_path: "%s/maps/%s" % (dir_path, STATS_FILE_NAME),
                                            ["/usr/local/dofus_tools", "/".join((os.getcwd(), "data"))])))
            if len(search_result) != 0:
                logging.info("Loading statistical analysis from %s" % search_result[0])
                with Path(search_result[0]).open("rb") as fi:
                    stats = StatisticalDataAnalysis.decode(fi.read())
            else:
                raise ValueError("Parametric frequency-file path \"-f / --frequency-file\" not provided and default analysis not found.")

    if input is None:
        print("No input specified. Nothing to do.")
        exit(0)
    if Path(input).is_file():
        raw_data_generator = (load_swf(input))
    elif Path(input).is_dir():
        raw_data_generator = SWFExplorer(input)
    else:
        raise FileNotFoundError("Input file or directory %s not found" % input)

    if not k_crack:
        unexpected_flags = set(k for k, be in ((("-e", "--exhaustive-length"), e_len),
                                               (("-m", "--max-keys"), m_k is not None),
                                               (("-f", "--frequency-file"), f_s is not None))
                               if be)
        if len(unexpected_flags) != 0:
            def arg_opts_repr(arg_opts):
                return "{%s}" % ", ".join("(%s)" % " / ".join("'%s'" % v for v in ao) for ao in arg_opts)
            logging.warning("The arument(s) %s were passed without setting the '-k' / '--key-crack' flag. Thus having no effect."
                            % arg_opts_repr(unexpected_flags))
    with Exporter(output) as exporter:
        workflow = []
        if k_crack:
            workflow.append(partial(key_crack_workflow,
                                    stats=stats,
                                    max_keys=m_k if m_k is not None else DEFAULT_KEY_COUNT,
                                    exhaustive_key_length=e_len))

        workflow.append(map_data_to_json)
        workflow.append(partial(Exporter.export, exporter))

        with Pool(worker_count if worker_count is not None else DEFAULT_THREADS_COUNT) as workers:
            workers.map(partial(chain, function_sequence=workflow), raw_data_generator)


if __name__ == "__main__":
    main()
