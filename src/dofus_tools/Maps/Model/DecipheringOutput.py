from typing import Dict, Any
from .Raw import Raw
from .DecipheringOptions import DecipheringOptions


class DecipheringOutput(object):
    """
    Model associating unaltered map data to a set of possible map content deciphering alternatives.
    """
    def __init__(self, raw_map: Raw, deciphered_options: DecipheringOptions):
        """
        Constructor.
        :param raw_map: Unaltered map data.
        :param deciphered_options: Set of possible map content deciphering alternatives
        """
        self.raw_map = raw_map
        self.deciphered_options = deciphered_options

    def to_json(self) -> Dict[str, Any]:
        """
        Encodes as a dictionary that can itself be json-encoded.
        :return: A dictionary that can be json-encoded.
        """
        return {**self.raw_map.to_json(), **self.deciphered_options.to_json()}

    @staticmethod
    def from_json(data: dict):
        """
        Decodes the DecipheringOutput from a dictionary.
        :param data: The dictionary encoded DecipheringOutput
        :return: The instance of DecipheringOutput corresponding to the encoded data
        """
        return DecipheringOutput(Raw.from_json(data), DecipheringOptions.from_json(data))