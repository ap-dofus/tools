from .Raw import Raw
from .Content import Content
from ..Key import Key


class Decoded(Raw):
    """
    Fully decoded map model.
    """
    def __init__(self, raw_map: Raw, key: Key):
        """
        Constructor
        :param raw_map: The Raw model containing raw data for the map.
        :param key: The key to decipher the map with.
        """
        Raw.__init__(self, raw_map.index, raw_map.date_tag, raw_map.coordinates_system.map_dimensions,
                     raw_map.background_num, raw_map.ambiance_id, raw_map.music_id, raw_map.outdoor,
                     raw_map.capabilities, raw_map.ciphered_data)

        self.content = Content.decode(key(self.ciphered_data, False))


