from .....Tools import extract_bits, SparseBytes
from .Layers import Layers
from .Movement import Movement


class Tile(object):
    """
    Structure representing the tile content of a Dofus map.
    """
    def __init__(self, los: bool, mvt: Movement, active: bool, layers: Layers):
        """
        Constructor
        :param los: Line of sight (whether a character / monster can use ranged attack over this tile or not).
        :param mvt: The movement capabilities of (over the) the tile.
        :param active: whether the tile is active or not.
        """
        self.los = los
        self.mvt = mvt
        self.active = active
        self.layers = layers

    def walkable(self):
        """
        Tests whether a tile can be walked over.
        :return: Whether the walkable instance is associated to a tile that can be walked over.
        """

        return self.active and self.mvt not in (Movement.NOT_WALKABLE, Movement.NOT_WALKABLE_INTERACTIVE)

    def encode(self) -> bytes:
        """
        Encodes the tile Data as base-64 to a byte-array.
        :return: base-64 encoded byte-array encoding the Tile data.
        """
        return bytes(SparseBytes.merge((
            SparseBytes({0: (1 if self.los else 0)}),
            SparseBytes({0: ((1 if self.active else 0) << 5)}),
            self.mvt.encode(),
            self.layers.encode()
        )))

    @staticmethod
    def decode(data):
        """
        Decodes a base-64 encoded tile data to a Tile instance.
        :param data: base-64 encoded tile data.
        :return: The decoded instance of Tile corresponding to the data.
        """

        return Tile(los=extract_bits(data[0], 0, 1) == 1,
                    mvt=Movement.decode(data),
                    active=extract_bits(data[0], 5, 1) == 1,
                    layers=Layers.decode(data))
