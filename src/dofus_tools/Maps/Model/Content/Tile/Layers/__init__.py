from ......Tools import SparseBytes
from .Ground import Ground
from .Overlay import Overlay
from .Interactive import Interactive


class Layers(object):
    """
    Layers data container for a single Dofus map tile
    """
    def __init__(self, ground: Ground, overlay: Overlay, interactive: Interactive):
        """
        Constructor
        :param ground: Layer of the tile containing data for the ground.
        :param overlay: Layer of the tile containing data for the overlay.
        :param interactive: Layer of the tile containing data for the interactive objects.
        """
        self.ground = ground
        self.overlay = overlay
        self.interactive = interactive

    def encode(self) -> SparseBytes:
        """
        Encodes the Layers data to the base-64 encoded byte-array of the tile data.
        :return: Sparse base-64 encoded byte-array of the tile data encoding the Layers data.
        """
        return SparseBytes.merge((layer.encode() for layer in (self.ground, self.overlay, self.interactive)))

    @staticmethod
    def decode(data: bytes):
        """
        Decodes the layers info from a base-64 encoded tile data.
        :param data: base-64 encoded tile data.
        :return: The decoded instance of Layers corresponding to the data.
        """
        return Layers(Ground.decode(data),
                      Overlay.decode(data),
                      Interactive.decode(data))
