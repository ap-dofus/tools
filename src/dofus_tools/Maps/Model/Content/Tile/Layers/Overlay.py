from ......Tools import extract_bits, SparseBytes
from .RotatedSpriteData import RotatedSpriteData


class Overlay(RotatedSpriteData):
    """
    Structure containing the data for the overlay-layer of a tile.
    """
    def encode(self) -> SparseBytes:
        """
        Encodes the overlay-layer data to the base-64 encoded byte-array of the tile data.
        :return: Sparse base-64 encoded byte-array of the tile data encoding the Overlay data.
        """
        res = SparseBytes()
        res.add_bits(0, extract_bits(self.index, 13, 1) << 2)
        res.add_bits(4, extract_bits(self.index, 12, 1))
        res.add_bits(5, extract_bits(self.index, 6, 6))
        res.add_bits(6, extract_bits(self.index, 0, 6))
        res.add_bits(7, extract_bits(self.rotation, 0, 2) << 4)
        res.add_bits(7, (1 if self.is_flipped else 0) << 3)
        return res

    @staticmethod
    def decode(data: bytes):
        """
        Decodes the overlay-layer info from a base-64 encoded tile data.
        :param data: base-64 encoded tile data.
        :return: The decoded instance of Overlay corresponding to the data.
        """
        return Overlay(index=sum((extract_bits(data[0], 2, 1) << 13,
                                  extract_bits(data[4], 0, 1) << 12,
                                  extract_bits(data[5], 0, 6) << 6,
                                  extract_bits(data[6], 0, 6))),
                       is_flipped=extract_bits(data[7], 3, 1) == 1,
                       rotation=extract_bits(data[7], 4, 2))
