from ......Tools import extract_bits, SparseBytes
from .SimpleSpriteData import SimpleSpriteData


class Interactive(SimpleSpriteData):
    """
    Structure containing the data for the layer of a tile containing objects that can be interacted with.
    """
    def __init__(self, index: int, is_flipped: bool, is_interactive: bool):
        SimpleSpriteData.__init__(self, index, is_flipped)
        """
        Constructor
        :param interactive: Whether the tile contains an object that can be interacted with or not.
        """
        self.is_interactive = is_interactive

    def encode(self) -> SparseBytes:
        """
        Encodes the interactive-layer data to the base-64 encoded byte-array of the tile data.
        :return: Sparse base-64 encoded byte-array of the tile data encoding the Interactive data.
        """
        res = SparseBytes()
        res.add_bits(0, extract_bits(self.index, 13, 1) << 1)
        res.add_bits(7, (1 if self.is_flipped else 0) << 2)
        res.add_bits(7, (1 if self.is_interactive else 0) << 1)
        res.add_bits(7, extract_bits(self.index, 12, 1))
        res.add_bits(8, extract_bits(self.index, 6, 6))
        res.add_bits(9, extract_bits(self.index, 0, 6))
        return res

    @staticmethod
    def decode(data):
        """
        Decodes the interactive-layer info from a base-64 encoded tile data.
        :param data: base-64 encoded tile data.
        :return: The decoded instance of Interactive corresponding to the data.
        """
        return Interactive(index=sum((extract_bits(data[0], 1, 1) << 13,
                                      extract_bits(data[7], 0, 1) << 12,
                                      extract_bits(data[8], 0, 6) << 6,
                                      extract_bits(data[9], 0, 6))),
                           is_flipped=extract_bits(data[7], 2, 1) == 1,
                           is_interactive=extract_bits(data[7], 1, 1) == 1)
