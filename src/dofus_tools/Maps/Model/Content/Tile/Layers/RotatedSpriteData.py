from .SimpleSpriteData import SimpleSpriteData

ALLOWED_ROTATIONS = {0, 1, 2, 3}


class RotatedSpriteData(SimpleSpriteData):
    """
    Base class for tile-related sprites with rotation.
    """
    def __init__(self, index: int, is_flipped: bool, rotation: int):
        SimpleSpriteData.__init__(self, index, is_flipped)
        # TODO: Check & document possible values for rotation.
        """
        Constructor
        :param rotation: The rotation of the sprite {0: unrotated, 1: ?, 2: ?, 3: ?}.
        """
        if rotation not in ALLOWED_ROTATIONS:
            raise ValueError("Invalid rotation value. Allowed values are %s" % repr(ALLOWED_ROTATIONS))
        self.rotation = rotation
