class SimpleSpriteData(object):
    """
    Base class for tile-related sprites.
    """
    def __init__(self, index: int, is_flipped: bool):
        """
        Constructor
        :param index: Index of the sprite.
        :param is_flipped: Whether the sprite is flipped or not.
        """
        self.index = index
        self.is_flipped = is_flipped
