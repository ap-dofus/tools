from ......Tools import extract_bits, SparseBytes
from .RotatedSpriteData import RotatedSpriteData


class Ground(RotatedSpriteData):
    """
    Structure containing the data for the ground-layer of a tile.
    """
    def __init__(self, index: int, is_flipped: bool, rotation: int, height: int, slope: int):
        RotatedSpriteData.__init__(self, index, is_flipped, rotation)
        # TODO: Test, Check & document possible values for slope.
        """
        Constructor
        :param height: The height of the ground of the tile.
        :param slope: The slope direction of the ground the tile {0: ?, 1: flat, 2: ?, 3: ?, 4: ?}. 
        """
        self.height = height
        self.slope = slope

    def encode(self) -> SparseBytes:
        """
        Encodes the ground-layer data to the base-64 encoded byte-array of the tile data.
        :return: Sparse base-64 encoded byte-array of the tile data encoding the Ground data.
        """
        res = SparseBytes()
        res.add_bits(0, extract_bits(self.index, 9, 2) << 3)
        res.add_bits(1, extract_bits(self.rotation, 0, 2) << 4)
        res.add_bits(1, extract_bits(self.height, 0, 4))
        res.add_bits(2, extract_bits(self.index, 6, 3))
        res.add_bits(3, extract_bits(self.index, 0, 6))
        res.add_bits(4, extract_bits(self.slope, 0, 4) << 2)
        res.add_bits(4, (1 if self.is_flipped else 0) << 1)
        return res

    @staticmethod
    def decode(data: bytes):
        """
        Decodes the ground-layer info from a base-64 encoded tile data.
        :param data: base-64 encoded tile data.
        :return: The decoded instance of Ground corresponding to the data.
        """
        return Ground(index=sum((extract_bits(data[0], 3, 2) << 9,
                                 extract_bits(data[2], 0, 3) << 6,
                                 extract_bits(data[3], 0, 6))),
                      is_flipped=extract_bits(data[4], 1, 1) == 1,
                      rotation=extract_bits(data[1], 4, 2),
                      height=extract_bits(data[1], 0, 4),
                      slope=extract_bits(data[4], 2, 4))
