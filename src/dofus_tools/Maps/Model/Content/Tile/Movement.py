from enum import Enum
from .....Tools import extract_bits, SparseBytes


class Movement(Enum):
    """
    Enumeration of possibilities for availability of movement over a tile.
    """
    NOT_WALKABLE = 0              # Simple not walkable tile.
    NOT_WALKABLE_INTERACTIVE = 1  # Not walkable tile containing an interactive object.
    TRIGGER = 2                   # Movement for triggers. Note: some triggers have the "DEFAULT" movement. This value is not reliable for detect triggers.
    LESS_WALKABLE = 3             # Walkable tiles, but not prioritized.
    DEFAULT = 4                   # Default movement value for walkable tiles.
    PADDOCK = 5                   # Tiles for paddocks.
    ROAD = 6                      # Tiles for a road. Those cells are prioritized.
    MOST_WALKABLE = 7             # The highest movement value, the most prioritized walkable tiles.

    def encode(self) -> SparseBytes:
        """
        Encodes the Movement layer data to the base-64 encoded byte-array of the tile data.
        :return: Sparse base-64 encoded byte-array of the tile data encoding the Movement data.
        """
        return SparseBytes({2: extract_bits(self.value, 0, 3) << 3})

    @staticmethod
    def decode(data: bytes):
        """
        Decodes the movement capability of a tile from a base-64 encoded tile data.
        :param data: base-64 encoded tile data.
        :return: The decoded instance of Movement corresponding to the data.
        """
        return Movement(extract_bits(data[2], 3, 3))
