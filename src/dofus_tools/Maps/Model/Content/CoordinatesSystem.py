from typing import Union, Tuple, Iterable, Dict


class CoordinatesSystem(object):
    """
    Tool to switch coordinates systems for tiles (tile-index <-> iso-grid[x, y]) in a map of specific dimensions.
    """
    def __init__(self, map_dimensions: Tuple[int, int]):
        """
        Constructor
        :param map_dimensions: (witdh, height) dimension of the map.
        """
        self.map_dimensions = map_dimensions
        self.__line_pair_len = self.map_dimensions[0] * 2 - 1

    def __call__(self, tile: Union[int, Tuple[int, int]]) -> Union[Tuple[int, int], int]:
        """
        Convert from index to coordinates, or from coordinates to index.
        :param tile: Either a tile index, or a tile coordinate.
        :return: Either a tile index, or a tile coordinate (opposite of the parameter).
        """
        if isinstance(tile, int):
            if not 0 <= tile < len(self):
                raise KeyError("Invalid tile index: %i. Valid indices are [0, %i]" % (tile, len(self) - 1))
            y = (tile // self.__line_pair_len) \
              - ((tile % self.__line_pair_len) % self.map_dimensions[0])
            return tuple(((tile - (self.map_dimensions[0] - 1) * y) // self.map_dimensions[0],
                          y))
        elif isinstance(tile, tuple):
            line_i = sum(tile)
            line_pair_i = line_i // 2
            inline_coordinates = -tile[1] + line_pair_i + (line_i % 2) * self.map_dimensions[0]
            index = self.__line_pair_len * line_pair_i + inline_coordinates
            if not 0 <= index < len(self):
                raise KeyError("Invalid tile coordinates: %s." % (repr(tile)))
            return index

    def __len__(self) -> int:
        """
        Length operator
        :return: The count of tile coordinates in the map.
        """
        return self.__line_pair_len * self.map_dimensions[1] - (self.map_dimensions[0] - 1)

    def indices(self) -> Iterable[int]:
        """
        Ordered generator of tile-indices in the map.
        :return: An iterable of ordered tile-indices contained in the map.
        """
        for i in range(len(self)):
            yield i

    def coordinates(self):
        """
        Generator of tile-coordinates in the map (Ordered by tile indices).
        :return: An iterable of tile-coordinates contained in the map (Ordered by tile indices).
        """
        for i in self.indices():
            yield self(i)

    def neighbors(self, tile: Union[Tuple[int, int], int], as_indices: bool=None) -> Iterable[Union[Tuple[int, int], int]]:
        """
        Generator of tiles neighboring a single tile.
        :param tile: The single tile to generate neighbors for. Can be a tile-index (int), or a cels-coordinate ((int, int)).
        :param as_indices: Whether to generate neighbor tiles as indices or coordinates (same as cell parameter type if not specified).
        :return: An iterable of tile-[index | coordinates] that are direct neighbor of the cell parameter.
        """
        if isinstance(tile, tuple):
            as_indices = as_indices if as_indices is not None else False
        elif isinstance(tile, int):
            as_indices = as_indices if as_indices is not None else True
            tile = self(tile)
        else:
            raise ValueError("Unexpected type for tile: %s. Valid types are int (cell-index) and tuple (cell-coordinates)")

        for n_c in (tuple(n_c_v + n_c_d for n_c_v, n_c_d in zip(tile, c_d))
                    for c_d in ((0, 1), (0, -1), (1, 0), (-1, 0))):
            try:
                n_c_i = self(n_c)  # Always try to cast, no matter the generator type, to ensure the tile is in the map.
            except KeyError:
                continue  # The tile is not in the map: do not generate (/yield).

            yield n_c_i if as_indices else n_c

    def to_json(self) -> Dict[str, int]:
        """
        Encodes as a dictionary that can itself be json-encoded.
        :return: A dictionary that can be json-encoded.
        """
        return {
            'width': self.map_dimensions[0],
            'height': self.map_dimensions[1]
        }

    @staticmethod
    def from_json(data: dict):
        """
        Decodes the CoordinatesSystem from a dictionary.
        :param data: The dictionary encoded CoordinatesSystem.
        :return: The instance of CoordinatesSystem corresponding to the encoded data.
        """
        return CoordinatesSystem((data['width'], data['height']))
