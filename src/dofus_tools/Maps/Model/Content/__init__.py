from typing import List
from ....Tools import b64e2ascii, ascii2b64e
from .Tile import Tile
from .CoordinatesSystem import CoordinatesSystem


class Content(List[Tile]):
    """
    Structure representing the content (list of tiles) of a Dofus map.
    """

    def encode(self) -> bytes:
        """
        Encodes the map Content as ASCII to a byte-array.
        :return: ASCII encoded byte-array encoding the Content.
        """
        return b64e2ascii(bytes().join(map(Tile.encode, self)))

    @staticmethod
    def decode(data: bytes):
        """
        Decodes an ASCII encoded map content to a map Content instance.
        :param data: ASCII encoded map content.
        :return: The decoded instance of Content corresponding to the data.
        """
        if len(data) % 10 != 0:
            raise ValueError("Map data length must be multiple of 10. Got: %i" % len(data))

        data = ascii2b64e(data)

        return Content(map(Tile.decode, (data[i:i+10] for i in range(0, len(data), 10))))
