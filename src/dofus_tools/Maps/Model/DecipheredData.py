from typing import Dict, Any
from ..Key.Key import Key
from .Data import Data


class DecipheredData(object):
    """
    Model to represent a single key / deciphering possibility.
    """
    def __init__(self, key: Key, deciphered: Data, confidence: float):
        """
        Constructor.
        :param key: The possible key.
        :param deciphered: The deciphered data associated to the key.
        :param confidence: The confidence level of the algorithm (arbitrary scale, based on statistical analysis).
        """
        self.key = key
        self.deciphered = deciphered
        self.confidence = confidence

    def to_json(self) -> Dict[str, Any]:
        """
        Encodes as a dictionary that can itself be json-encoded.
        :return: A dictionary that can be json-encoded.
        """
        return {
            'key': self.key.encode(),
            'clear_data': "".join(map(chr, self.deciphered)),
            'confidence': self.confidence,
        }

    @staticmethod
    def from_json(data: dict):
        """
        Decodes the DecipheredData from a dictionary.
        :param data: The dictionary encoded DecipheredData.
        :return: The instance of DecipheredData corresponding to the encoded data.
        """
        return DecipheredData(Key.decode(data['key']),
                              Data(map(ord, data['clear_data'])),
                              data['confidence'])
