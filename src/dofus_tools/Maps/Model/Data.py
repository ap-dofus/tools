from ...Tools import hex2bytes, bytes2hex
from ...Crypto import DataFrequencies


class Data(bytes):
    """
    Model for raw map content data.
    """
    def is_invalid(self, stat: DataFrequencies) -> bool:
        """
        Check whether the (presumably deciphered/clear) raw map content data contains illegal bytes.
        :param stat: The absolute statistical analysis of a representative dataset.
        :return: Check whether the raw map content data contains illegal bytes.
        """
        return any(c not in stat for c in self)

    def encode(self) -> str:
        """
        Encodes the raw map content data as hex values.
        :return: The hex encoded raw map content.
        """
        return bytes2hex(self)

    @staticmethod
    def decode(hex_data: str):
        """
        Decodes the raw map content data from hex values.
        :param hex_data: The hex values to decode the raw map content data from.
        :return: The instance of Data decoded from hex_data.
        """
        return Data(hex2bytes(hex_data))
