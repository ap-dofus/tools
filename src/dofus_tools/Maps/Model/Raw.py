from typing import Tuple, Dict, Any, Union
from .Data import Data
from .Content import CoordinatesSystem


class Raw(object):
    """
    Model representing map data as parsed from a .swf file (/unaltered).
    """
    def __init__(self, index: int, date_tag: str, size: Union[Tuple[int, int], CoordinatesSystem], background_num: int,
                 ambiance_id: int, music_id: int, outdoor: bool, capabilities: int, ciphered_data: Data):
        """
        Constructor.
        :param index: Index of the map in the Dofus DB.
        :param date_tag: date_tag of the map in the dofus DB.
        :param size: Size of the map (witdh, height).
        :param background_num: Index of the background for the map.
        :param ambiance_id: TODO ?
        :param music_id: Index of the music to be heard in the map.
        :param outdoor: Whether the map is outdoor or not.
        :param capabilities: TODO ?
        :param ciphered_data: The ciphered raw map content data.
        """
        self.index = index
        self.date_tag = date_tag
        self.coordinates_system = CoordinatesSystem(size) if isinstance(size, tuple) else size
        self.background_num = background_num
        self.ambiance_id = ambiance_id
        self.music_id = music_id
        self.outdoor = outdoor
        self.capabilities = capabilities
        self.ciphered_data = ciphered_data

    def to_json(self) -> Dict[str, Any]:
        """
        Encodes as a dictionary that can itself be json-encoded.
        :return: A dictionary that can be json-encoded.
        """
        return {
            'id': self.index,
            'date_tag': self.date_tag,
            **self.coordinates_system.to_json(),
            'background_num': self.background_num,
            'ambiance_id': self.ambiance_id,
            'music_id': self.music_id,
            'outdoor': self.outdoor,
            'capabilities': self.capabilities,
            'ciphered_data': self.ciphered_data.encode()
        }

    @staticmethod
    def from_json(data: dict):
        """
        Decodes the Raw map from a dictionary.
        :param data: The dictionary encoded Raw map.
        :return: The instance of Raw map corresponding to the encoded data.
        """
        return Raw(data['id'], data['date_tag'], CoordinatesSystem.from_json(data), data['background_num'],
                   data['ambiance_id'], data['music_id'], data['outdoor'], data['capabilities'],
                   Data.decode(data['ciphered_data']))

    def __hash__(self) -> int:
        """
        Hash operator.
        :return: The hash according to the index and date_tag of the instance.
        """
        return hash((self.index, self.date_tag))

    def __eq__(self, other) -> bool:
        """
        Equality operator.
        :param other: The other Raw instance to test equality with.
        :return: Whether both raw instances have the same index and date_tag or not.
        """
        return self.index == other.index and self.date_tag == other.date_tag
