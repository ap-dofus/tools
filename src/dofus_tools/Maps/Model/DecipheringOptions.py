from typing import List, Dict, Any
from .DecipheredData import DecipheredData


class DecipheringOptions(List[DecipheredData]):
    """
    Model to represent a set of key / deciphering possibilities (presumably associated to a Raw map data).
    """
    def to_json(self) -> Dict[str, List[Dict[str, str]]]:
        """
        Encodes as a dictionary that can itself be json-encoded.
        :return: A dictionary that can be json-encoded.
        """
        return {'solutions': list(map(DecipheredData.to_json, self))}

    @staticmethod
    def from_json(data: dict):
        """
        Decodes the DecipheringOptions from a dictionary.
        :param data: The dictionary encoded DecipheringOptions.
        :return: The instance of DecipheringOptions corresponding to the encoded data.
        """
        return DecipheringOptions(map(DecipheredData.from_json, data['solutions']))
