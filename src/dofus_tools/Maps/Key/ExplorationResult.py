from typing import Dict, List, Iterator, Tuple
import itertools
import math
from statistics import mean

from ...Tools import zip_map
from .Key import Key


class ExplorationResult(object):
    """
    Container for the result of a key exploration.
    """

    def __init__(self, res: List[Dict[int, int]]):
        """
        Constructor
        :param res: Key exploration result (sum of (probabilistic error | for each decoded bytes) | for each byte possibility | for each byte of the key).
        """
        self.res = res

    def bytewise_prune(self, tgt_len):
        """
        Prune the candidate values of each byte to keep the N best values.
        :param tgt_len: Maximum candidate values to keep for each byte of the key.
        """
        self.res = [dict(zip_map(d.get, list(sorted(d.keys(), key=d.get)[:tgt_len])))
                    for d in self.res]

        return self

    def prune(self, tgt_len):
        """
        Prune the result to keep only (AT MOST) the N best key bytes combinations.
        :param tgt_len: Maximum candidate keys to keep.
        """
        ratios = [{i: float(s) / b for i, s in d.items()}
                  for b, d in zip((min(d.values()) for d in self.res), self.res)]
        while self.__len__() > tgt_len:
            max_oi = max((i for i in range(len(ratios)) if len(ratios[i]) != 1), key=lambda i: max(ratios[i].values()))
            max_ci = max(ratios[max_oi].keys(), key=ratios[max_oi].get)
            del ratios[max_oi][max_ci]
            del self.res[max_oi][max_ci]

        return self

    def best(self) -> Tuple[Key, float]:
        """
        Accessor for the best candidate key.
        :return: Key instance representing the best candidate, and the probabilistic error associated to it.
        """
        k = Key(bytes(min(d.keys(), key=d.get) for d in self.res))
        return k, self.__eval_key(k)

    def __eval_key(self, raw_key) -> float:
        """
        Key evaluation method: Average the probabilistic error sums associated to each byte of the key.
        :param raw_key: The key to evaluate.
        :return: The Average of the probabilistic error sums associated to each byte of raw_key.
        """
        return mean(float(d[p]) for d, p in zip(self.res, raw_key))

    def __len__(self) -> int:
        """
        Length operator.
        :return: The amount of candidates keys (remaining) in the result.
        """
        return math.prod(len(d) for d in self.res)

    def __iter__(self) -> Iterator[Tuple[Key, float]]:
        """
        Iteration operator.
        :return: A generator of all key candidates, and the probabilistic error associated to them, ordered from best (lowest) to worst.
        """
        for k in sorted(itertools.product(*(list(d.keys()) for d in self.res)),
                        key=lambda pl: self.__eval_key(list(pl))):
            yield Key(bytes(k)), self.__eval_key(k)
