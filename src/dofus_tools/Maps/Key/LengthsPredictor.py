from typing import Iterator
from functools import reduce
from statistics import mean

from ...Tools import zip_map, coincidence_rate
from ...Crypto import hamming_xor_distance_bytes as _hamming_dist
from ...Crypto import key_index_data_map, data_block


class LengthsPredictor(object):
    """
    Tool to predict the length for the key ciphering a given data.
    """
    def __init__(self, min_v=128, max_v=256):
        """
        Constructor.
        :param min_v: Minimum (inclusive) key length.
        :param max_v: Maximum (inclusive) key length.
        """
        self.min_v = min_v
        self.max_v = max_v

    def kasiski_test_predict(self, d: bytes, minimum_pattern_len=3) -> int:
        """
        Predict the key-length using the Kasiski test.
        :param d: The ciphered data for which to predict the key length.
        :param minimum_pattern_len: The minimum length of a repeating sequence for it to be considered a pattern.
        :return: The key-length estimated using the Kasiski test
        """
        def GCD(a, b):
            if self.min_v <= a <= self.max_v:
                return a
            if self.min_v <= b <= self.max_v:
                return b
            if 0 in (a, b):
                return a | b

            min_v, max_v = min(a, b), max(a, b)
            return GCD(min_v, max_v % min_v)

        shifts = set()

        for shift in range(1, len(d)):
            match_count = 0
            for i in range(0, len(d) - shift):
                if d[i] == d[i + shift]:
                    match_count += 1
                    if match_count >= minimum_pattern_len:
                        shifts.add(shift)
                else:
                    match_count = 0

        return reduce(GCD, filter(lambda v: v >= self.min_v, shifts))

    def friedman_test_predict(self, d: bytes) -> int:
        """
        Predict the key-length using the Friedman test.
        :param d: The ciphered data for which to predict the key length.
        :return: The key-length estimated using the Friedman test
        """
        tests = {k_l: mean(coincidence_rate(key_index_data_map(d, k_i, k_l))
                           for k_i in range(k_l))
                 for k_l in range(self.min_v, self.max_v + 1)}

        return max(tests.keys(), key=tests.get)

    def hamming_distance_predict(self, data: bytes) -> int:
        """
        Predict the key-length using the lowest repeating hamming distance.
        :param d: The ciphered data for which to predict the key length.
        :return: The key-length estimated using the hamming distances.
        """
        data_len = len(data)
        dists_map = {
            k_l: sum(_hamming_dist(*d) for d in
                     map(lambda t: tuple((data_block(data, v, k_l) for v in t)),
                         ((x, y) for x in range(blocks_count - 1) for y in range(x + 1, blocks_count)))) / data_len
            for k_l, blocks_count in zip_map(lambda v: data_len // v, range(self.min_v, self.max_v + 1))
        }
        return min(dists_map.keys(), key=dists_map.get)

    def possible_key_length(self, data, use_friedman=True, use_kasiski=True, use_hamming=True, exhaustive=False)\
            -> Iterator[int]:
        """
        Generates an ordered set of key-lengths for the key ciphering a given data.
        :param data: The ciphered data for which to predict the key length.
        :param use_friedman: Whether to use the Friedman test for the prediction or not.
        :param use_kasiski: Whether to use the Kasiski test for the prediction or not.
        :param use_hamming: Whether to use the Hamming distance for the prediction or not.
        :param exhaustive: Whether to generate all possible key lengths or not.
        :return:
        """
        # Container to keep track of generated keys.
        generated = set()

        possible_generators = [(use_friedman, self.friedman_test_predict),
                               (use_kasiski, self.kasiski_test_predict),
                               (use_hamming, self.hamming_distance_predict)]
        for f, g in possible_generators:
            if f:
                k_l = g(data)
                if self.min_v <= k_l <= self.max_v and k_l not in generated:
                    generated.add(k_l)
                    yield k_l

        if exhaustive:
            # Iterate over all possible key lengths, but avoid repeating those already generated.
            for i in set(range(self.min_v, self.max_v + 1)) - generated:
                yield i

