from ...Crypto import PositionalDataFrequencies, key_index_data_map, key_error_for_index
from .ExplorationResult import ExplorationResult


class Explorer(object):
    """
    Tool to explore all the values for all the bytes for a given key-length and ciphered map-content data.
    """
    def __init__(self, stats: PositionalDataFrequencies, alphabet: set = None):
        """
        Constructor.
        :param stats: Position-based statistical analysis of a representative dataset.
        :param alphabet: The alphabet of the key bytes.
        """
        self.alphabet = set(range(32, 255)) if alphabet is None else alphabet
        self.stats = stats

    def explore_keys(self, data, key_len) -> ExplorationResult:
        """
        Explore all the values for all the bytes for a given key-length and ciphered map-content data.
        :param data: The ciphered map-content data to explore keys for.
        :param key_len: The length of the keys to be explored.
        :return: The result of the key exploration.
        """
        return ExplorationResult(
            [{x: key_error_for_index(bytes(x ^ b for b in key_index_data_map(data, k_i, key_len)),
                                     self.stats, k_i, key_len)
              for x in self.alphabet}
             for k_i in range(key_len)])
