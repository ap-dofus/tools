from typing import Union
from urllib.parse import unquote_plus, quote

from ...Crypto import XORCipher
from ...Tools import NIBBLE_CHECKSUM,\
                    left_memory_rotation, right_memory_rotation,\
                    hex2bytes, bytes2hex
from ..Model import Data


class Key(XORCipher):
    """
    XOR Cipher and Key tools, specialized for handling map-content data from the Dofus game.
    """
    @staticmethod
    def encoding_escape(data: bytes) -> bytes:
        # TODO Maybe this should be more generic? As it may also be used for encoding the ciphered map data.
        """
        Escapes characters when encoding the key-data
        :param data: The encoded data.
        :return: The encoded data with some characters escaped.
        """

        return bytes(eb for b in data for eb in ([b] if (32 <= b <= 127) and b not in (37, 43)
                                                 else map(ord, quote(chr(b)))))

    def __init__(self, key: bytes):
        """
        Constructor.
        :param key: The key to use for ciphering / deciphering map-content data.
        """
        XORCipher.__init__(self, key)

    def __len__(self) -> int:
        """
        Length operator.
        :return: The length of the key.
        """
        return len(self.key)

    def encode(self) -> str:
        """
        Encodes the key instance to hex (right memory rotation + hex conversion)
        :return: The hex encoded (+rotated) key
        """
        return bytes2hex(Key.encoding_escape(right_memory_rotation(self.key, NIBBLE_CHECKSUM(self.key) * 2)))

    @staticmethod
    def decode(hex_key: str):
        """
        Decodes a key instance from hex (hex conversion + left memory rotation)
        :param hex_key: The hex encoded (+rotated) key
        :return: The Key instance decoded from the hex_key.
        """
        shifted_key = bytes(map(ord, unquote_plus("".join(map(chr, hex2bytes(hex_key))))))
        return Key(left_memory_rotation(shifted_key, NIBBLE_CHECKSUM(shifted_key) * 2))

    def __call__(self, data: Union[str, bytes, Data], output_hex=None) -> Union[str, Data]:
        """
        Ciphers / Deciphers the map-content data.

        :param data: Ciphered or clear map-content data.
        :return: Ciphered data if the parameter was clear data. Clear data if the parameter was ciphered data.
        """

        if isinstance(data, str):
            output_hex = output_hex if output_hex is not None else True
            data = hex2bytes(data)
        else:
            output_hex = output_hex if output_hex is not None else False

        res = XORCipher.__call__(self, data)

        if output_hex:
            res = "".join(map(chr, res))
        else:
            res = Data(res)

        return res
