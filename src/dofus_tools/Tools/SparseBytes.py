from functools import reduce, partial
from typing import Dict, Iterator
from .Functional import zip_map


class SparseBytes(Dict[int, int]):
    """
    Tool to help build byte-arrays in a segmented manner.
    """

    @staticmethod
    def ensure_byte(val: int):
        """
        Ensures that the value of an integer can fit in a byte.
        :param val: The integer value to check size for.
        """
        if not 0 <= val <= 255:
            raise ValueError("Invalid byte value: %i. Byte values can only be in [0; 255].")

    def __setattr__(self, key, value):
        """
        Array setter operator
        :param key: Index of the byte to set value for.
        :param value: Byte value to associate at the specified index.
        """
        SparseBytes.ensure_byte(value)
        Dict[int, int].__setattr__(self, key, value)

    def add_bits(self, key: int, value: int, fail_on_intersection: bool=True):
        """
        Add bits to (possibly) existing byte.
        :param key: Index of the byte to add bits to.
        :param value: Byte value representing the bits to add at the specified index.
        :param fail_on_intersection: Whether an exception should be thrown if existing and additional bits were to intersect / collide or not.
        """
        SparseBytes.ensure_byte(value)
        if key not in self:
            self[key] = value
        else:
            if fail_on_intersection:
                if self[key] & value != 0:
                    raise ValueError("Intersection between existing content (%i) and additional value (%i) during bites addidion for byte %i"
                                     % (self[key], value, key))
            self[key] |= value

    def __iter__(self) -> Iterator[int]:
        """
        Iterator for all (including not set) bytes of the (not so) sparse byte-array.
        :return: An iterator of byte (int).
        """
        for i in range(max(self.keys()) + 1):
            if i in self:
                yield self[i]
            else:
                yield 0

    def __len__(self):
        """
        Length operator.
        :return: The length of the full byte-array associated to this sparse byte-array.
        """
        return max(self.keys()) + 1

    @staticmethod
    def merge_op(a, b, fail_on_intersection=True):
        """
        Binary merge operator for sparse byte-arrays.
        :param a: The (sparse byte-array) left hand side of the merge operator.
        :param b: The (sparse byte-array) right hand side of the merge operator.
        :param fail_on_intersection: Whether an exception should be thrown if bits were to intersect / collide or not.
        :return: The merged sparse byte-array.
        """
        result = SparseBytes()
        a_keys, b_keys = set(a.keys()), set(b.keys())

        # Left difference.
        for k, v in zip_map(a.__getitem__, a_keys - b_keys):
            result[k] = v
        # Right difference.
        for k, v in zip_map(b.__getitem__, b_keys - a_keys):
            result[k] = v
        # Intersection.
        for k, (v_a, v_b) in zip_map(lambda key: tuple((a[key], b[key])), a_keys & b_keys):
            if fail_on_intersection:
                intersection = v_a & v_b
                if intersection != 0:
                    raise ValueError("Intersection between bytes n°%i (a:%i, b:%i) during sparse byte-array merging"
                                     % (k, v_a, v_b))
            result[k] = v_a | v_b

        return result

    @staticmethod
    def merge(sparse_bytes_iterable, fail_on_intersection=True):
        """
        Tool to merge a set of sparse byte-arrays.
        :param sparse_bytes_iterable: The iterable sequence of sparse byte-arrays to merge.
        :param fail_on_intersection: Whether an exception should be thrown if bits were to intersect / collide or not.
        """
        return reduce(partial(SparseBytes.merge_op, fail_on_intersection=fail_on_intersection),
                      sparse_bytes_iterable,
                      SparseBytes())
