import functools


def zip_map(f, l):
    """
    Zips each value of an iterable with the output of a function-call with said value as parameter.
    :param f: The function to map values with.
    :param l: The values to zip-map
    :return: A generator of (value, f(value)).
    """

    for k in l:
        yield k, f(k)


def chain(input_value, function_sequence):
    """
    Chain a value through a sequence of function call.
    :param input_value: The initial value to chain.
    :param function_sequence: The sequence of function.
    :return: The output of the call of the last function in the sequence.
    """
    return functools.reduce(lambda v, f: f(v), function_sequence, input_value)
