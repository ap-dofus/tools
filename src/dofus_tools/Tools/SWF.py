import logging
import xml.etree.ElementTree as ElementTree


def load_xml_encoded_swf(input_path: str, action_type=None) -> dict:
    """
    Loads the dictionary of data contained in the .xml file resulting from a swf2xml conversion.
    :param input_path: The path of the .xml resulting from a swf2xml conversion.
    :param action_type: The type of action to extract dictionaries for.
    :return: dictionary of data contained in the .xml file at input_path.
    """
    tree = ElementTree.parse(input_path)

    dicts = []

    for i, tag in enumerate(tree.getroot().find("Header").find("tags").findall("DoAction")):
        actions = tag.find("actions")
        stack_dict = [n.get("value") for n in actions.find("Dictionary").find("strings")]
        def parse_data_node(dn):
            if dn.tag == "StackInteger":
                return int(dn.attrib["value"])
            if dn.tag == "StackBoolean":
                return int(dn.attrib["value"]) == 1
            elif dn.tag in ("StackDictionaryLookup", "StackLargeDictionaryLookup"):
                return stack_dict[int(dn.attrib["index"])]

        def data_generator():
            e = [n for n in actions if n.tag != "Dictionary"]
            for a_e, a_en in zip(e[:-1], e[1:]):
                if action_type is not None:
                    if a_en.tag not in action_type:
                        continue
                if a_e.tag == "PushData":
                    try:
                        res = tuple(parse_data_node(n) for n in a_e.find("items"))

                        if len(res) < 2:
                            raise ValueError("Not enough items found while parsing %s" % input_path)
                        yield res[0], res[1:]
                    except Exception as e:
                        logging.debug("Error while parsing %s: %s" % (input_path, str(e)))

        dicts.append({k: v for k, v in data_generator()})

    return dicts
