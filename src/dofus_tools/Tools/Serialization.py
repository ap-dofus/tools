HEX_CHARS = set(hex(i)[-1].upper() for i in range(16))


def hex2bytes(hex_seq: str) -> bytes:
    """
    Decodes byte-array from a hex string.
    :param hex_seq: Encoded hex string
    :return: The decoded byte-array.
    """
    hex_seq = hex_seq.upper()
    invalid = set(c for c in hex_seq if c not in HEX_CHARS)
    if len(invalid) != 0:
        raise ValueError("%s are not hexadecimal characters." % repr(invalid))
    if len(hex_seq) % 2 != 0:
        raise ValueError("The hex sequence length must be pair (1 Byte = 2 hex char).")
    return bytes(int(hex_seq[i: i + 2], 16) for i in range(0, len(hex_seq), 2))


def bytes2hex(byte_seq: bytes) -> str:
    """
    Encodes a byte-array to a hex string
    :param byte_seq: The byte-array to encode.
    :return: The encoded hex string.
    """
    def hex_repr(v: int):
        if not (0 <= v <= 255):
            raise ValueError("%s is an invalid valus for a Byte ([0, 255])")
        return hex(v)[2:].rjust(2, '0')

    invalid = set(v for v in byte_seq if not (0 <= v <= 255))
    if len(invalid):
        raise ValueError("%s are invalid values for byte type." % repr(invalid))
    return "".join(map(hex_repr, byte_seq)).upper()


BASE64E_ALPHABET = list(map(ord, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"))
BASE64E_ALPHABET_INDEX = {b: i for i, b in enumerate(BASE64E_ALPHABET)}


def b64e2ascii(b64e_seq: bytes) -> bytes:
    """
    Encodes a base 64 byte-array to an ASCII encoded byte-array
    :param b64e_seq: The base 64 byte-array to encode.
    :return: The ASCII encoded byte array.
    """
    return bytes(map(BASE64E_ALPHABET.__getitem__, b64e_seq))


def ascii2b64e(ascii_seq: bytes) -> bytes:
    """
    Decodes a base 64 byte-array from an ASCII encoded byte-array.
    :param ascii_seq: ASCII encoded byte array.
    :return: The decoded base 64 byte-array.
    """
    invalid = set(chr(b) for b in ascii_seq if b not in BASE64E_ALPHABET_INDEX)
    if len(invalid):
        raise ValueError("%s are not valid letters for base 64 encoded words." % repr(invalid))
    return bytes(map(BASE64E_ALPHABET_INDEX.get, ascii_seq))
