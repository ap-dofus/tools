from typing import Dict, Union, Iterable, Any


def elements_count(l: Iterable[Any]) -> Dict[Any, int]:
    """
    Count the appearance of each element in a sequence.
    :param l: The sequence to count elements from.
    :return: A dictionary of {element: count} associated to the sequence.
    """
    d = dict()
    for e in l:
        d[e] = d.get(e, 0) + 1
    return d


def elements_frequencies(l: Iterable[Any]) -> Dict[Any, float]:
    """
    Compute the frequency of each element in a sequence.
    :param l: The sequence to compute the frequency of elements from.
    :return: A dictionary of {element: frequency} associated to the sequence.
    """
    d = dict()
    s = 0
    for e in l:
        d[e] = d.get(e, 0) + 1
        s += 1
    return {k: float(v) / s for k, v in d.items()}


def coincidence_rate(arg: Union[Dict[int, float], Iterable[int], bytes]):
    """
    Evaluates the frequency distribution
    :param arg: Frequency count or sequence of data.
    :return: The coincidence rate found in the frequency count or sequence of data.
    """
    if isinstance(arg, dict):
        counts = arg
        data_len = sum(counts.values())
    elif isinstance(arg, bytes):
        counts = elements_count(arg)
        data_len = len(arg)
    else:
        raise ValueError("Unhandled arg type: %s" % type(arg))

    return sum(map(lambda c: (c * (c - 1)), counts.values())) / (data_len * (data_len - 1))
