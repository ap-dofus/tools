from functools import reduce


class CheckSum(object):
    """
    Tool to compute the checksum of a byte array for a given checksum-type-size.
    """
    def __init__(self, bit_size: int):
        """
        Constructor.
        :param bit_size: The size of the checksum type (!in bits!)
        """
        self.max_val = 2 ** bit_size

    def __call__(self, data: bytes) -> int:
        """
        Computes the checksum.
        :param data: The data to compute the checksum for.
        :return: The checksum for data.
        """
        return reduce(lambda a, b: (a + b) % self.max_val, data, 0)


# Tool to compute the checksum as a nibble (4 bits / half a byte)
NIBBLE_CHECKSUM = CheckSum(4)
