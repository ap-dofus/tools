def left_rotate(d: bytes, count=1):
    """
    Rotates the bytes in a byte-array to the left.
    :param d: The byte-array to rotate.
    :param count: The count of (single-step) left rotations to execute.
    :return: The rotated byte-array
    """
    count %= len(d)
    return d[count:] + d[0:count]


def right_rotate(d: bytes, count=1):
    """
    Rotates the bytes in a byte-array to the right.
    :param d: The byte-array to rotate.
    :param count: The count of (single-step) right rotations to execute.
    :return: The rotated byte-array
    """
    count %= len(d)
    return d[len(d) - count:] + d[0:len(d) - count]


def extract_bits(bits_seq: int, pos: int, count: int) -> int:
    """
    Extract an ordered sequence of bits from a byte.
    :param bits_seq: The initial sequence of bits from which to extract the bit sub-sequence from.
    :param pos: The position in the initial sequence of bits of the first bit of the sub-sequence.
    :param count: The length (count of bits) of the subsequence to extract.
    :return: A bit-sequence starting with the extracted sub-sequence.
    """
    return (bits_seq >> pos) & int("".join("1" for _ in range(count)), 2)
