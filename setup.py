import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

with open("requirements.whl.txt", "r", encoding="utf-8") as fh:
    requirements = [r for r in fh.read().split('\n') if len(r) > 0 and r[0] != "#"]

setuptools.setup(
    name="dofus_tools",
    version="0.1",
    author="Axel Paccalin",
    author_email="axel.paccalin@gmail.com",
    description="A python package to manipulate data from the Dofus game.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ap-dofus/tools",
    project_urls={
        "Bug Tracker": "https://gitlab.com/ap-dofus/tools/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=requirements,
    data_files=[('dofus_tools', ['LICENSE', 'requirements.whl.txt', 'README.md']),
                ('dofus_tools/man', ['man/README-MAPS-CONVERT.md',
                                     'man/README-MAPS-DISPLAY.md',
                                     'man/README-GENERATE-LANG.md']),
                ('dofus_tools/maps', ["data/maps/data-statistics.json"])]
)
