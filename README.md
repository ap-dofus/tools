#Dofus Tools package.
A python package to manipulate data from the Dofus game.

## Dependencies
### System (to be installed manually):
 * [python3](https://www.python.org/downloads/) (interpreter)
 * [python3-pip](https://pip.pypa.io/en/stable/installation/) (package + dependencies install)
 * [swfmill](https://www.swfmill.org/) (.swf <-> .xml)

### Python (installed automatically):
 * [pygame](https://www.pygame.org) (map display)
 * [requests](https://pypi.org/project/requests/) (lang files download)

## Install from self-hosted pypi repository
```bash
  python3 -m pip install --upgrade --extra-index-url https://pypi.axel-paccalin.com/simple dofus_tools 
```

## Package & Install from sources
### Test
```bash
    python3 -m python3 -m unittest discover tests
```
### Package
You will also need the `wheel` and `build` python packages for that:
```bash
    python3 -m pip install --upgrade wheel build
```
The following will create both legacy and wheel packages in a `dist` directory:
```bash
    python3 -m build 
```
### Install
```bash
    python3 -m pip install --upgrade ./dist/dofus_tools-<X.X>-py3-none-any.whl
```

    
## Tool-specific readmes:
 * [maps-convert](man/README-MAPS-CONVERT.md)
 * [maps-display](man/README-MAPS-DISPLAY.md)
 * [generate-lang](man/README-GENERATE-LANG.md)

## Functionalities (available & planned / considered):
 * [ ] Maps
   * [x] IO.
     * [x] .swf Extraction
     * [x] .json Export.
     * [x] .json Import
   * [x] Deciphering.
   * [x] Key generation:
     * [x] Statistical crack 
   * [ ] Content handling
     * [x] modeling.
     * [x] parsing.
     * [ ] display
       * [x] Walkable cells
       * [x] Obstructed (LOS) cells.
       * [ ] Elevation
       * [ ] Slope
       * [ ] Sprites (would need sprite-loading)
         * [ ] Ground
         * [ ] Overlay
         * [ ] Interactive
 * [ ] Lang
   * [ ] Server-Data.
     * [ ] modeling
     * [x] parsing (kind of)
     * [x] export (kind of)
     * [ ] edition
       * [x] Registration urls
       * [x] Lobby server host
       * [x] Lobby server listening port.
       * [ ] Game server name.
       * [ ] Game server description.
       