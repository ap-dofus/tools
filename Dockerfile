FROM python:3.9-slim

ARG PACKAGES_LOCATION
COPY ${PACKAGES_LOCATION} /packages
# Get the image up to date
RUN apt update && apt upgrade --yes
RUN python3 -m pip install --upgrade pip

# Install system requirements
COPY requirements.dpkg.txt /requirements.dpkg.txt
RUN cat ./requirements.dpkg.txt | xargs apt install -yqq

# Install the dofus_tools package
RUN python3 -m pip install --find-links=/packages dofus_tools
RUN rm -rf /packages

# Image size optimization
RUN apt-get remove python3-pip --yes
RUN apt-get clean autoclean
RUN apt-get autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/

# Workdir & mountpoint
RUN mkdir /dofus_tools
WORKDIR /dofus_tools
VOLUME /dofus_tools

# Command to run on container start
ENTRYPOINT [ "python3", "-m", "dofus_tools"]
CMD [ "" ]
