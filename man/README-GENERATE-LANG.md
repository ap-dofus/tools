# Usage example of the generate-lang tool.
These files are necessary for the CDN of self-hosted emulators.

## Download the lang from the official Ankama CDN
```bash
python3 -m dofus_tools generate-lang -v info
``` 

## Download the lang from the official Ankama CDN and edit it to apply a specific server config.
```bash
python3 -m dofus_tools generate-lang -v info -c examples/lang-generator-config.json
``` 

## Complete manual
```bash
python3 -m dofus_tools generate-lang [-h / --help] 
```