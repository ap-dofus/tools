# Usage example of the maps-convert tool.
## Obtaining the dofus game data:
You can download it from the [official Ankama launcher](https://www.ankama.com/en/launcher), by installing the ___dofus-retro___ game. 
The game data files will then be located in `%AppData%\Local\Ankama\Retro\ressources\app\retroclient\data`

You can also download a client release from the [Dofedex](https://github.com/dofera/dofedex) archive.
In which case, the game data files will be located in `<extracted-client>/data`.

## Generating keys
```bash
python3 -m dofus_tools maps-convert -w -v info -x <nb-threads> -o <output-directory> <dofus-data-location>/maps
``` 
###Arguments:
  * `-k` Activates the key generation. By default (without it), the `maps` tool will only convert raw .swf data to .json
  * `-v info` Logs more detailed infos than the default (warning) level. 
  * `-w <nb-threads>` Splits the load across multiple python processes (default 1). For max performance, use <nb-threads> = amount of virtual cpu cores on host.
  * `-o <output-directory>` Directory into which to save the processed data (raw map data + predicted keys). 
  * `<dofus-data-location>/maps` Directory containing map data encoded as .swf files. Can also be a single .swf file. 

You can split the content of the positional input parameter `<dofus-data-location>/maps` beforehand and do multiple run. If you choose to try to predict keys for all the available maps, the data parsing step could take some time (up to a few minutes for configs with slow file-systems).

### Performances
On a high-end computer with a multitasking-oriented cpu, using the default statistical analysis, you can expect to have predictions for all the maps (more than 10k) in about one hour.

### Key validity
Keys are predicted from probabilities generated with known data. The confidence level that the generator associates to each of them is based on these probabilities.
Depending on possible bias in the probability analysis, and the originality of any given map-data, it is possible that the generator will never manage to find the correct key for said data.


## Complete manual
```bash
python3 -m dofus_tools maps-convert [-h / --help] 
```