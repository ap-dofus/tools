# Usage example of the maps-display tool.
## Obtaining deciphered data
You can generate key candidates with the `-k` flag of the [map-convert tool](README-MAPS-CONVERT.md).
You can also specify a hex encoded key manually with the `-k` option.

## Displaying the sketch of the map
```bash
python3 -m dofus_tools maps-display <converted_map_file_path>.json
``` 

## Complete manual
```bash
python3 -m dofus_tools maps-display [-h / --help] 
```