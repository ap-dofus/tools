FROM python:3.10-slim-bullseye

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --upgrade wheel build

RUN apt clean autoclean
RUN apt autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/
