FROM python:3.10-slim-bullseye

COPY requirements.dpkg.txt .
COPY requirements.whl.txt .

RUN apt update
RUN cat ./requirements.dpkg.txt | xargs apt install -yqq
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.whl.txt

RUN apt clean autoclean
RUN apt autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}/
